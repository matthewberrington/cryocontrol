import numpy as np

class DiodeThermometer:
    def __init__(self, calibration):
        self.V_T_interp = calibration
        self.T_history = [np.zeros(3000)]
        self.T_history = np.reshape(self.T_history,(1000,3))

    def V_to_T(self, V):
        if V < 527.9: #below range 
            ret  = 300
        elif 527.9 < V < 1855.15776: #in range of voltage for which the calibration is known
            ret = float(self.V_T_interp(V))
        else:
            ret  = 300 #above range, return 300 as must be disconnected
        return ret

    def V_error_to_T_error(self, V, V_error):
        error_pos = np.abs(self.V_to_T(V-V_error)-self.V_to_T(V))
        error_neg = np.abs(self.V_to_T(V+V_error)-self.V_to_T(V))
        return [error_neg, error_pos]

    def set_voltage(self, V_input):
        V, V_error = V_input
        self.current_V = V
        self.current_V_error = V_error
        
        self.current_T = self.V_to_T(self.current_V)
        self.current_T_error_neg, self.current_T_error_pos = self.V_error_to_T_error(self.current_V, self.current_V_error)

        self.T_history = np.roll(self.T_history,-3)
        self.T_history[-1] = np.array([self.current_T, self.current_T_error_neg, self.current_T_error_pos])

    @property
    def T(self):
        return self.current_T

class Thermistor:
    def __init__(self, Voltage_to_Resistance_calibrated, Voltage_to_Resistance_error_calibrated, calibration):
        self.R_T_interp = calibration
        self.V_to_R = Voltage_to_Resistance_calibrated
        self.V_error_to_R_error = Voltage_to_Resistance_error_calibrated
        self.T_history = [np.zeros(3000)]
        self.T_history = np.reshape(self.T_history,(1000,3))

    def R_to_T(self, R):
        if 1000 < R < 9868: #in range of resistances for which the calibration is known
            ret  = float(self.R_T_interp(R))
        else:
            ret  = 300 #If out of range, just return room temp
        return ret

    def R_error_to_T_error(self, R, R_error):
        error_pos = np.abs(self.R_to_T(R-R_error)-self.R_to_T(R))
        error_neg = np.abs(self.R_to_T(R+R_error)-self.R_to_T(R))
        return error_neg, error_pos

    def set_voltage(self, V_input):
        V, V_error = V_input
        self.current_V = V
        self.current_V_error = V_error

        self.current_R = self.V_to_R(self.current_V)
        self.current_R_error = self.V_error_to_R_error(self.current_V, self.current_V_error)


        self.current_T = self.R_to_T(self.current_R)
        self.current_T_error_neg, self.current_T_error_pos = self.R_error_to_T_error(self.current_R, self.current_R_error)

        self.T_history = np.roll(self.T_history,-3)
        self.T_history[-1] = np.array([self.current_T, self.current_T_error_neg, self.current_T_error_pos])
        
    @property
    def T(self):
        return self.current_T