import serial
import struct
import time
import LivePlot
import threading



import logging
import logging.handlers
import os
import re

class ParallelTimedRotatingFileHandler(logging.handlers.TimedRotatingFileHandler):
    def __init__(self, filename, when='h', interval=1, backupCount=0, encoding=None, delay=False, utc=False, atTime=None, postfix = ".log"):

        self.origFileName = filename
        self.when = when.upper()
        self.interval = interval
        self.backupCount = backupCount
        self.utc = utc
        self.atTime = atTime
        self.postfix = postfix

        if self.when == 'S':
            self.interval = 1 # one second
            self.suffix = "%Y-%m-%d_%H-%M-%S"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}$"
        elif self.when == 'M':
            self.interval = 60 # one minute
            self.suffix = "%Y-%m-%d_%H-%M"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}$"
        elif self.when == 'H':
            self.interval = 60 * 60 # one hour
            self.suffix = "%Y-%m-%d_%H"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}_\d{2}$"
        elif self.when == 'D' or self.when == 'MIDNIGHT':
            self.interval = 60 * 60 * 24 # one day
            self.suffix = "%Y-%m-%d"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}$"
        elif self.when.startswith('W'):
            self.interval = 60 * 60 * 24 * 7 # one week
            if len(self.when) != 2:
                raise ValueError("You must specify a day for weekly rollover from 0 to 6 (0 is Monday): %s" % self.when)
            if self.when[1] < '0' or self.when[1] > '6':
                 raise ValueError("Invalid day specified for weekly rollover: %s" % self.when)
            self.dayOfWeek = int(self.when[1])
            self.suffix = "%Y-%m-%d"
            self.extMatch = r"^\d{4}-\d{2}-\d{2}$"
        else:
            raise ValueError("Invalid rollover interval specified: %s" % self.when)

        currenttime = int(time.time())
        logging.handlers.BaseRotatingHandler.__init__(self, self.calculateFileName(currenttime), 'a', encoding, delay)

        self.extMatch = re.compile(self.extMatch)
        self.interval = self.interval * interval # multiply by units requested

        self.rolloverAt = self.computeRollover(currenttime)

    def calculateFileName(self, currenttime):
        if self.utc:
             timeTuple = time.gmtime(currenttime)
        else:
             timeTuple = time.localtime(currenttime)

        return self.origFileName + "." + time.strftime(self.suffix, timeTuple) + self.postfix

    def getFilesToDelete(self, newFileName):
        dirName, fName = os.path.split(self.origFileName)
        dName, newFileName = os.path.split(newFileName)

        fileNames = os.listdir(dirName)
        result = []
        prefix = fName + "."
        postfix = self.postfix
        prelen = len(prefix)
        postlen = len(postfix)
        for fileName in fileNames:
            if fileName[:prelen] == prefix and fileName[-postlen:] == postfix and len(fileName)-postlen > prelen and fileName != newFileName:
                 suffix = fileName[prelen:len(fileName)-postlen]
                 if self.extMatch.match(suffix):
                     result.append(os.path.join(dirName, fileName))
        result.sort()
        if len(result) < self.backupCount:
            result = []
        else:
            result = result[:len(result) - self.backupCount]
        return result

    def doRollover(self):
         if self.stream:
            self.stream.close()
            self.stream = None

         currentTime = self.rolloverAt
         newFileName = self.calculateFileName(currentTime)
         newBaseFileName = os.path.abspath(newFileName)
         self.baseFilename = newBaseFileName
         self.mode = 'a'
         self.stream = self._open()

         if self.backupCount > 0:
             for s in self.getFilesToDelete(newFileName):
                 try:
                     os.remove(s)
                 except:
                     pass

         newRolloverAt = self.computeRollover(currentTime)
         while newRolloverAt <= currentTime:
             newRolloverAt = newRolloverAt + self.interval

         #If DST changes and midnight or weekly rollover, adjust for this.
         if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
             dstNow = time.localtime(currentTime)[-1]
             dstAtRollover = time.localtime(newRolloverAt)[-1]
             if dstNow != dstAtRollover:
                 if not dstNow:  # DST kicks in before next rollover, so we need to deduct an hour
                     newRolloverAt = newRolloverAt - 3600
                 else:           # DST bows out before next rollover, so we need to add an hour
                     newRolloverAt = newRolloverAt + 3600
         self.rolloverAt = newRolloverAt
         

class heater:
    def __init__(self):
        self.current = 0
        self.voltage = 0
        self.enabled = False
    
    def update(self,I,V,state):
        self.current = I
        self.voltage = V
        self.enabled = state
        
class heaters:
    def __init__(self):
        self.He4_pump = heater()
        self.He3_pump = heater()
        self.He4_switch = heater()
        self.He3_switch = heater()
    
    
        
        
class switches:
    def __init__(self):
        self.He4 = 0
        self.He3 = 0

class temperature:
    def __init__(self):
        self.He3_head = 100
        self.He4_head = 100
        self.Film_burner = 100
        self.He4_pump = 100
        self.He3_pump = 100
        self.He4_switch = 100
        self.He3_switch = 100


class cryocooler:
    def __init__(self):
        self.temperature = temperature()
        self.heaters = heaters()
        self.switches = switches()
        self.mode = 0
        self.flags = 0
        self.setpoint = 0 
        self.desorbing_elapsed = 0
        self.thermalising_elapsed = 0
        self.cooling_elapsed = 0
        self.running_elapsed = 0
    
    def update(self, data):
        self.temperature.He3_head = data[1]
        self.temperature.He4_head = data[2]
        self.temperature.Film_burner = data[3]
        self.temperature.He4_pump = data[4]
        self.temperature.He3_pump = data[5]
        self.temperature.He4_switch = data[6]
        self.temperature.He3_switch = data[7]
        
        self.heaters.He4_pump.update(*data[8:11])
        self.heaters.He3_pump.update(*data[11:14])
        self.heaters.He4_switch.update(*data[14:17])
        self.heaters.He3_switch.update(*data[17:20])
      
        self.switches.He4 = data[21]
        self.switches.He3 = data[22]
        self.mode = data[23]
        self.flags = data[24]
        self.setpoint = data[25]
        self.desorbing_timer = data[26]
        self.thermalising_timer = data[27]
        self.cooling_timer = data[28]
        self.running_timer = data[29]
        self.checksum = data[30]
        
    def print_state(self):
        
        print("{:7.3f}K, {:7.3f}K, {:7.3f}K, {:7.3f}K, {:7.3f}K, {:7.3f}K, {:7.3f}K, {:.3f}A, {:2.0f}V, {:1d}, {:.3f}A, {:2.0f}V, {:1d}, {:4d}, {:4d}, {:4d}, {:4d}, {:4d}, {:4d}, {:.3f}K, {:4d}s, {:4d}s, {:4d}s, {:4d}s".format(self.temperature.He3_head,self.temperature.He4_head,self.temperature.Film_burner,self.temperature.He4_pump, self.temperature.He3_pump, self.temperature.He4_switch, self.temperature.He3_switch, self.heaters.He4_pump.current, self.heaters.He4_pump.voltage, self.heaters.He4_pump.enabled, self.heaters.He3_pump.current, self.heaters.He3_pump.voltage, self.heaters.He3_pump.enabled, self.heaters.He4_switch.enabled, self.heaters.He3_switch.enabled, self.switches.He4, self.switches.He3, self.mode, self.flags, self.setpoint, self.desorbing_timer, self.thermalising_timer, self.cooling_timer, self.running_timer), end="\r")
        
        
        

def request_status():
    msg = b'\x00\x00\x00\x00\x00\x00\x96C\x00\x00\x96Ch\x10>C\xdd-\\Cc\xe02C9\xbe3C\x0e\xed+C\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xe2\x08\x00\x00'
    arduino.write(msg)

def set_config():
    msg = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    data = [0]*31
    data[0] = 2 # header
    data[1] = 0 # He3_head temperature
    data[2] = 0 # He4_head temperature
    data[3] = 0 # Film_burner temperature
    data[4] = 0 # He4_pump temperature
    data[5] = 0 # He3_pump temperature
    data[6] = 0 # He4_switch temperature
    data[7] = 0 # He3_switch temperature
    data[8] = 0.12 # He4_pump current
    data[9] = 25 # He4_pump voltage
    data[10] = 0 # He4_pump enabled
    data[11] = 0.050 # He3_pump current
    data[12] = 16 # He3_pump voltage
    data[13] = 0 # He3_pump enabled
    data[14] = 0 # He4_switch current
    data[15] = 0 # He4_switch voltage
    data[16] = 0 # He4_switch enabled
    data[17] = 0 # He3_switch current
    data[18] = 0 # He3_switch voltage
    data[19] = 0 # He3_switch enabled
    data[20] = 0 # heaters changed  
    data[21] = 0 # switches.He4
    data[22] = 0 # switches.He3
    data[23] = 0 # mode
    data[24] = 0 # flags
    data[25] = 0 # setpoint
    data[26] = 0 # desorbing_timer
    data[27] = 0 # thermalising_timer
    data[28] = 0 # cooling_timer
    data[29] = 0 # running_timer
    data[30] = 0 # checksum
    b = struct.pack('=IfffffffffIffIffIffIIIIIIfIIII',*(data[:-1]))
    byte_tuple = struct.unpack('=120B',b)
    data[30] = sum(byte_tuple[:-1])
    msg = struct.pack('=IfffffffffIffIffIffIIIIIIfIIIII',*data)
    arduino.write(msg)
    
    
    
    
def checksum_correct(serial_input):
    byte_tuple = struct.unpack('=120BI',serial_input)
    checksum = sum(byte_tuple[:-1])
    return checksum == byte_tuple[-1]

def receive_status():
    serial_input = arduino.read(124)
    if checksum_correct(serial_input):
        data = struct.unpack('=IfffffffffIffIffIffIIIIIIfIIIII',serial_input)
        return data
    else:
        print(serial_input)
        raise ValueError('Checksum not correct')
    
    
# # try:
if __name__ == '__main__':
    
    handler = ParallelTimedRotatingFileHandler('temperature',when='midnight')
    formatter = logging.Formatter(fmt='%(asctime)s,%(message)s', datefmt = '%H:%M:%S')
    handler.setFormatter(formatter)
    logger = logging.getLogger('log')
    logger.setLevel('INFO')
    logger.addHandler(handler)

    print("He3 Head, He4 Head, Film Bnr, He4 Pump, He3 Pump, He4 Swit, He3 Swit, He4 Pmp Heater, He3 Pmp Heater, 4htr, 3htr, 4stt, 3stt, Mode, Flag, Setpnt, desrb, therm,  cool, runng")
    cryocooler = cryocooler()
    arduino = serial.Serial('COM9', 115200, timeout=2)
    serial_lock = threading.Lock()
    run = threading.Event()
    run.set()
    s = LivePlot.Superplot("somePlot")
    q, q_terminate = s.start()

    t0=time.time()
    
    while True:
        # try:
        # set_config()
        # msg = arduino.read(100)
        request_status()
        
        data = receive_status()
        cryocooler.update(data)
        cryocooler.print_state()
        
        # except Exception as e:
        #     print(e)
        
        logger.info("{:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}, {:7.3f}".format(cryocooler.temperature.He3_head, cryocooler.temperature.He4_head, cryocooler.temperature.Film_burner, cryocooler.temperature.He4_pump, cryocooler.temperature.He3_pump, cryocooler.temperature.He4_switch, cryocooler.temperature.He3_switch))
        
        if run.is_set():
            q.put((time.time()-t0,cryocooler.temperature.He3_head, cryocooler.temperature.He4_head, cryocooler.temperature.Film_burner, cryocooler.temperature.He4_pump, cryocooler.temperature.He3_pump, cryocooler.temperature.He4_switch, cryocooler.temperature.He3_switch))
            # print(cryocooler.temperature.He3_head, cryocooler.temperature.He4_head, cryocooler.temperature.Film_burner, cryocooler.temperature.He4_pump, cryocooler.temperature.He3_pump, cryocooler.temperature.He4_switch, cryocooler.temperature.He3_switch)
        
        # time.sleep(1)
        
         
# except Exception as e:
#     print(e)
#     arduino.close()

# except KeyboardInterrupt:
#     arduino.close()