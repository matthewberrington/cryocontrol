#include <stdint.h>
// typedef struct {
//   union {
//     struct {
//       float a; 
//       float b;
//       float c;
//       float d;
//       float e;
//       float f;
//       float g;
//     };
//     float temps[7];
//   }
// } TEMPERATURES_T;

typedef struct {
  uint32_t He3_head; 
  uint32_t He4_head;
  uint32_t Film_burner;
  uint32_t He4_pump;
  uint32_t He3_pump;
  uint32_t He4_switch;
  uint32_t He3_switch;
} VOLTAGES_T;

typedef struct {
  float He3_head; 
  float He4_head;
  float Film_burner;
  float He4_pump;
  float He3_pump;
  float He4_switch;
  float He3_switch;
} TEMPERATURES_T;

typedef struct {  
  float current;
  float voltage;
  uint32_t enabled;
} HEATER_T;

typedef struct {
  HEATER_T He4_pump;
  HEATER_T He3_pump;
  HEATER_T He4_switch;
  HEATER_T He3_switch;
  uint32_t changed;
} HEATERS_T;

typedef struct {
  uint32_t He4;
  uint32_t He3;
} SWITCHES_T;

typedef enum MODE_T: uint32_t{
  IDLING=0,
  DESORBING,
  THERMALISING,
  COOLING,
  RUNNING,
} MODE_T;


typedef struct {
  uint32_t thermalising_complete : 1;
  uint32_t no_helium : 2;
} FLAGS_T;


typedef struct {
  TEMPERATURES_T temperatures;
  HEATERS_T heaters;
  SWITCHES_T switches;
  MODE_T mode;
  FLAGS_T flags;
  float setpoint;
  volatile uint32_t desorbing_elapsed;
  volatile uint32_t thermalising_elapsed;
  volatile uint32_t cooling_elapsed;
  volatile uint32_t running_elapsed;
} GLOBALS_T;


typedef enum MESSAGE_ID_T{
  MSG_GET_STATUS=0,
  MSG_STATUS,
  MSG_CONFIGURE,
} MESSAGE_ID_T;


typedef struct {
  uint32_t header; //4
  uint8_t data[sizeof(GLOBALS_T)]; //116
  uint32_t checksum; //4
} MESSAGE_T;
