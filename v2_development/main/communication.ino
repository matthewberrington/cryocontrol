void send_status(GLOBALS_T state){
  MESSAGE_T msg;
  msg.header = MSG_STATUS;
  memcpy(&msg.data, &state, sizeof(GLOBALS_T));
  msg.checksum = calc_CRC(msg);
  
  Serial.write((byte*)&msg, sizeof(msg));
  }

uint32_t calc_CRC(MESSAGE_T msg)
 {
   uint32_t sum = 0;
   uint8_t * p = (uint8_t*)&msg;
   for (uint8_t i=0; i<sizeof(MESSAGE_T)-4; i++)
   {
     sum += p[i];
   }
   return sum;
 }

int valid_message_received(MESSAGE_T * msg){
  int len = 0;
  char buf [sizeof(MESSAGE_T)] = {};
  if (Serial.available()){
    len = Serial.readBytes(buf, 1000);
    memcpy(msg,&buf,sizeof(MESSAGE_T));
  }
  return (len == sizeof(MESSAGE_T)) &&  (msg -> checksum == calc_CRC(*msg));
}


void execute_request(MESSAGE_T request, GLOBALS_T * state){
  MESSAGE_ID_T request_type =  (MESSAGE_ID_T) request.header;

  if (request_type == MSG_GET_STATUS){
    send_status(*state);
  } 
  else if (request_type == MSG_CONFIGURE){
//      Serial.println("Set config");
    set_configuration(request, state);
  }
}  
