int multiplier;


VOLTAGES_T measure_voltages() {
  int num_measurements = 250*20;//Make this number a multiple of measurement_per_cycle. 20*250 = 1000ms of measurements
  int measurement_per_cycle = 476; //Make this number even. Each cycle is 200us. 500 = 10Hz
  VOLTAGES_T volt = {0}; 
  long timer_start = millis();
  for (int i=0; i<num_measurements; i++){
  
    if (i % measurement_per_cycle == 0) {
      digitalWrite(ACDrivePin, HIGH);
      multiplier = 1;
    }
    else if (i % measurement_per_cycle == measurement_per_cycle/2) {
      digitalWrite(ACDrivePin, LOW);
      multiplier = -1;
    }
    
    volt.He3_head    += multiplier * analogRead(voltAPin);
    volt.He4_head    += multiplier * analogRead(voltBPin);
    volt.Film_burner += analogRead(voltCPin);
    volt.He4_pump    += analogRead(voltDPin);
    volt.He3_pump    += analogRead(voltEPin);
    volt.He4_switch  += analogRead(voltFPin);
    volt.He3_switch  += analogRead(voltGPin);  
    delayMicroseconds(110);   
    
  }
  
  volt.He3_head = 2*volt.He3_head/num_measurements*3300000.0/8191.0;
  volt.He4_head    = 2*volt.He4_head/num_measurements*3300000.0/8191.0;
  volt.Film_burner = volt.Film_burner/num_measurements*3300000.0/8191.0;
  volt.He4_pump    = volt.He4_pump/num_measurements*3300000.0/8191.0;
  volt.He3_pump    = volt.He3_pump/num_measurements*3300000.0/8191.0;
  volt.He4_switch  = volt.He4_switch/num_measurements*3300000.0/8191.0;
  volt.He3_switch  = volt.He3_switch/num_measurements*3300000.0/8191.0;
  return volt;  
}

void measure_temperature(GLOBALS_T * state){
  VOLTAGES_T volt = measure_voltages();
  TEMPERATURES_T temps;
  double resistance_He4;
  double resistance_He3;
  resistance_He4 = 1.3387753721881892*volt.He4_head - 124.47354417022443;  //from calibration 07/05/19
  resistance_He3 = 4.438715402514645*volt.He3_head - 65.45195638779182;   //from calibration 07/05/19
  temps.He3_head    = linearInterpolate(resistance_He3, temperature_vs_resistance,sizeof(temperature_vs_resistance)/sizeof(temperature_vs_resistance[0]));
  temps.He4_head    = linearInterpolate(resistance_He4, temperature_vs_resistance,sizeof(temperature_vs_resistance)/sizeof(temperature_vs_resistance[0]));
  temps.Film_burner = linearInterpolate(volt.Film_burner, temperature_vs_voltage,sizeof(temperature_vs_voltage)/sizeof(temperature_vs_voltage[0]));
  temps.He4_pump    = linearInterpolate(volt.He4_pump, temperature_vs_voltage,sizeof(temperature_vs_voltage)/sizeof(temperature_vs_voltage[0]));
  temps.He3_pump    = linearInterpolate(volt.He3_pump, temperature_vs_voltage,sizeof(temperature_vs_voltage)/sizeof(temperature_vs_voltage[0]));
  temps.He4_switch  = linearInterpolate(volt.He4_switch, temperature_vs_voltage,sizeof(temperature_vs_voltage)/sizeof(temperature_vs_voltage[0]));
  temps.He3_switch  = linearInterpolate(volt.He3_switch, temperature_vs_voltage,sizeof(temperature_vs_voltage)/sizeof(temperature_vs_voltage[0]));
  state -> temperatures = temps;
  update_switch_status(state);
}
