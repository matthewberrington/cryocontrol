//#include <stdint.h>
//#include "cooldown.h"

void idle(GLOBALS_T * state){
}

void desorb(GLOBALS_T * state){
  if (state -> switches.He4 || state -> switches.He3) {
    // A switch is still on, make sure the switch heaters are off
    state -> heaters.He4_switch.enabled = 0;
    state -> heaters.He3_switch.enabled = 0;
  } else {
    warm_He4_pump(state);
    warm_He3_pump(state);
  }  

  if (state -> temperatures.He4_pump < 50.0 || state -> temperatures.He3_pump < 45.0){
    // Pumps are not warm, reset desorption timer
    state -> desorbing_elapsed = 0;
  }

  if (state -> desorbing_elapsed > 60*30){
    // Desorbtion process complete
    state -> mode = COOLING;
    state -> cooling_elapsed = 0;
  }
}

void thermalise(GLOBALS_T * state){
  if (state -> switches.He4 || state -> switches.He3) {
    // A switch is still on, make sure the switch heaters are off
    state -> heaters.He4_switch.enabled = 0;
    state -> heaters.He3_switch.enabled = 0;
  } else {
    warm_He4_pump(state);
    warm_He3_pump(state);
  }  

  if (state -> temperatures.He3_head > 6.0 || state -> temperatures.He4_head > 6.0){
    // Cold heads are still warm, reset termalisation timer
    state -> thermalising_elapsed = 0;
  }

  if (state -> thermalising_elapsed > 60*10){
    // Thermalisation process complete
    state -> mode = DESORBING;
    state -> desorbing_elapsed = 0;
  }
  
}

void cooldown(GLOBALS_T * state){
  uint32_t run_begin;
  

  if (state -> cooling_elapsed < 5*60 && state -> heaters.He4_switch.enabled == 0){
    // Turn off He4 pump
    state -> heaters.He4_pump.enabled = 0;
    warm_He3_pump(state);
    state -> heaters.He4_switch.enabled = 0;
    state -> heaters.He3_switch.enabled = 0;
  } 

  if (state -> temperatures.He4_head >= 2.0 || state -> temperatures.He3_head >= 2.0){
    // Turn on He4 switch
    state -> heaters.He4_pump.enabled = 0;
    warm_He3_pump(state);
    state -> heaters.He4_switch.enabled = 1;
    state -> heaters.He3_switch.enabled = 0;   
    state -> cooling_elapsed = 0;
  }
  
   if (state -> cooling_elapsed < 5*60 && state -> temperatures.He4_head < 2.0 && state -> temperatures.He3_head < 2.0){
    // Turn off He3 pump
    state -> heaters.He4_pump.enabled = 0;
    state -> heaters.He3_pump.enabled = 0;
    state -> heaters.He4_switch.enabled = 1;
    state -> heaters.He3_switch.enabled = 0;
    return;
  }
  
  if (state -> cooling_elapsed > 5*60 && state -> temperatures.He4_head < 2.0 && state -> temperatures.He3_head < 2.0){
    // Turn on He3 switch
    state -> heaters.He4_pump.enabled = 0;
    state -> heaters.He3_pump.enabled = 0;
    state -> heaters.He4_switch.enabled = 1;
    state -> heaters.He3_switch.enabled = 1;
    run_begin = state -> cooling_elapsed;
  }

  if (state -> cooling_elapsed - run_begin > 30*60){
    // Cooldown complete
    state -> mode = RUNNING;
  }
}

void runn(GLOBALS_T * state){
  state -> heaters.He4_pump.enabled = 0;
  state -> heaters.He4_switch.enabled = 1;
  state -> heaters.He3_switch.enabled = 1;

  He3head_input = state -> temperatures.He3_head;
  He3head_setpoint = state -> setpoint;
  execute_He3head_PID(state);
//  He3head_output
}

void update_switch_status(GLOBALS_T * state){
  if (state -> temperatures.He4_switch > 10.0){
    state -> switches.He4 = 1;
  } else {
    state -> switches.He4 = 0;
  }

  if (state -> temperatures.He3_switch > 10.0){
    state -> switches.He3 = 1;
  } else {
    state -> switches.He3 = 0;
  }
}

void set_configuration(MESSAGE_T request, GLOBALS_T * state){
  GLOBALS_T * requested_state = (GLOBALS_T*) &request.data;
  state -> mode = requested_state -> mode;
  if (requested_state -> mode == IDLING){
    state -> heaters = requested_state -> heaters;
  }
}
