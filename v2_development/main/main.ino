#include "typedefs.h"

#include <stdint.h>
#include <PID_v1.h>
#include <Metro.h>

#define POWERSUPPLY Serial1
#define SEGMENT Serial2
Metro timersMetro = Metro(1000);

GLOBALS_T stat;

const int He3switchPin = 6;
const int He4switchPin = 5;


//Define variables we'll be connecting to our PIDs
double He3pump_input, He3pump_output, He3pump_setpoint;
double He4pump_input, He4pump_output, He4pump_setpoint;
double He3head_input, He3head_output, He3head_setpoint;


//Specify the links and initial tuning parameters for PIDs
//Reversed as increase current = increase temperature = decrease resistance
PID He3pump_PID(&He3pump_input, &He3pump_output, &He3pump_setpoint, 0.004, 0.0003, 0.0, DIRECT);
PID He4pump_PID(&He4pump_input, &He4pump_output, &He4pump_setpoint, 0.010, 0.0005, 0.0, DIRECT);
PID He3head_PID(&He3head_input, &He3head_output, &He3head_setpoint, 30.0, 3.0, 0.0, DIRECT);

const int voltAPin = 16;
const int voltBPin = 15;
const int voltCPin = 21;
const int voltDPin = 20;
const int voltEPin = 19;
const int voltFPin = 18;
const int voltGPin = 17;
const int ACDrivePin = 23;

void setup() {
  Serial.begin(9600); // open the serial port at 115200 bps:  
    //open serial port to power supply
  POWERSUPPLY.begin(19200, SERIAL_8N1);
  SEGMENT.begin(9600, SERIAL_8N1);
//  SEGMENT.begin(9600, SERIAL_8N1);
  Serial.setTimeout(100);
  //put teensy ADC into high resolution mode
  analogReadRes(13);
  stat.mode = DESORBING;
  He3pump_PID.SetMode(AUTOMATIC);
  He4pump_PID.SetMode(AUTOMATIC);

    // prepare all used teensy pins
  pinMode(voltAPin, INPUT);
  pinMode(voltBPin, INPUT);
  pinMode(voltCPin, INPUT);
  pinMode(voltDPin, INPUT);
  pinMode(voltEPin, INPUT);
  pinMode(voltFPin, INPUT);
  pinMode(voltGPin, INPUT);
  pinMode(ACDrivePin, OUTPUT);
  pinMode(He3switchPin, OUTPUT);
  digitalWrite(He3switchPin, LOW);
  pinMode(He4switchPin, OUTPUT);
  digitalWrite(He4switchPin, LOW);
}

void loop ()
{
  measure_temperature(&stat);  //takes 1 second
  numeric_message(stat.temperatures.He3_head);
//  single_message("chur");
//  single_message("IdLE");
  
  MESSAGE_T request = {0};
  if (valid_message_received(&request)){
    execute_request(request,&stat);
  }

  if (timersMetro.check() == 1){
    stat.desorbing_elapsed++;
    stat.thermalising_elapsed++;
    stat.cooling_elapsed++;
    stat.running_elapsed++;
  }

  switch (stat.mode){
    case IDLING:
      idle(&stat);
      break;
    case DESORBING:
      desorb(&stat);
      break;
    case THERMALISING:
      thermalise(&stat);
      break;
    case COOLING:
      cooldown(&stat);
      break;
    case RUNNING:
      runn(&stat);
      break;
  }
  set_heaters(&stat);
}
