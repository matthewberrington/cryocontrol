#define PID_CONTROL_H


void execute_He3pump_PID(GLOBALS_T * state) {
  He3pump_PID.Compute();
  state -> heaters.He3_pump.current = He3pump_output;
  state -> heaters.He3_pump.voltage = 16;  //set limit voltage supply to 16V,  incase the PID says outputs something stupid
}

void execute_He4pump_PID(GLOBALS_T * state) {
  He4pump_PID.Compute();
  state -> heaters.He4_pump.current = He4pump_output;
  state -> heaters.He4_pump.voltage = 25;  //set limit voltage supply to 25V,  incase the PID says outputs something stupid
}

void execute_He3head_PID(GLOBALS_T * state) {
  He3head_PID.Compute();
  state -> heaters.He3_pump.current = 0.050;
  state -> heaters.He3_pump.current = He3head_output;
}


void warm_He4_pump(GLOBALS_T * state) {
  state -> heaters.He4_pump.enabled = 1;
  if (state -> temperatures.He4_pump < 40.0) {
    // Rapid heat mode
    state -> heaters.He4_pump.current = 0.130;
    state -> heaters.He4_pump.voltage = 28;
  } else {
    //do PID stuff
    He4pump_setpoint = 55.0;
    He4pump_input = state -> temperatures.He4_pump;
    execute_He4pump_PID(state);
  }
}

void warm_He3_pump(GLOBALS_T * state) {
  state -> heaters.He3_pump.enabled = 1;
  if (state -> temperatures.He3_pump < 35.0) {
    // Rapid heat mode
    state -> heaters.He3_pump.current = 0.060;
    state -> heaters.He3_pump.voltage = 16;
  } else {
    //do PID stuff
    He3pump_setpoint = 48.0;
    He3pump_input = state -> temperatures.He3_pump;
    execute_He3pump_PID(state);
  }
}

void set_heaters(GLOBALS_T * state) {

  String He3voltage_command;
  String He3current_command;
  String He4voltage_command;
  String He4current_command;

  //Power supplies wants 3 dp  
  He3voltage_command = "V1 " + String(state -> heaters.He3_pump.voltage, 3).substring(0, 5) + "\n";
  He3current_command = "I1 " + String(state -> heaters.He3_pump.current, 3).substring(0, 5) + "\n";
  He4voltage_command = "V2 " + String(state -> heaters.He4_pump.voltage, 3).substring(0, 5) + "\n";
  He4current_command = "I2 " + String(state -> heaters.He4_pump.current, 3).substring(0, 5) + "\n";
  
  POWERSUPPLY.print("OP1 " +  String(state -> heaters.He3_pump.enabled) + "\n");
  POWERSUPPLY.print(He3voltage_command);
  POWERSUPPLY.print(He3current_command);

  POWERSUPPLY.print("OP2 " +  String(state -> heaters.He4_pump.enabled) + "\n");
  POWERSUPPLY.print(He4voltage_command);
  POWERSUPPLY.print(He4current_command);

  POWERSUPPLY.print("OP3 1\n");
  digitalWrite(He3switchPin, state -> heaters.He3_switch.enabled);
  digitalWrite(He4switchPin, state -> heaters.He4_switch.enabled);

}
