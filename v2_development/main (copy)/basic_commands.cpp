//#include <stdint.h>
//#include "typedefs.h"

uint8_t	 He3_switch_is_on(GLOBALS_T status) {
  return status.temperatures.He3_switch > 12.0;
}

uint8_t	 He4_switch_is_on(GLOBALS_T status) {
  return status.temperatures.He4_switch > 12.0;
}
