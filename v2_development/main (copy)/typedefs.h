#include <stdint.h>
// typedef struct {
//   union {
//     struct {
//       float a; 
//       float b;
//       float c;
//       float d;
//       float e;
//       float f;
//       float g;
//     };
//     float temps[7];
//   }
// } TEMPERATURES_T;

typedef struct {
  float He3_head; 
  float He4_head;
  float Film_burner;
  float He4_pump;
  float He3_pump;
  float He4_switch;
  float He3_switch;
} TEMPERATURES_T;

typedef struct {
  float current;
  uint8_t enabled:1;
} HEATER_T;

typedef struct {
  HEATER_T He4_pump;
  HEATER_T He3_pump;
  HEATER_T He4_switch;
  HEATER_T He3_switch;
  uint8_t changed:1;
} HEATERS_T;

typedef enum {
  IDLING=0,
  DESORBING,
  THERMALISING,
  COOLING,
  RUNNING,
} MODE_T;

typedef struct {
  uint8_t no_helium:1;
} FLAGS_T;


typedef struct {
  TEMPERATURES_T temperatures;
  HEATERS_T heaters;
  MODE_T mode;
  FLAGS_T flags;
  float setpoint;
  uint32_t desorbing_elapsed;
  uint32_t thermalising_elapsed;
  uint32_t cooling_elapsed;
  uint32_t running_elapsed;
} GLOBALS_T;


typedef enum {
  MSG_GET_STATUS,
  MSG_STATUS,
  MSG_CONFIGURE,
} MESSAGE_ID_T;



typedef struct {
  uint16_t header;
  uint8_t length;
  uint8_t * data;
  uint16_t crc;
} MESSAGE_T;