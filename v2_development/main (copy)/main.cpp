#include "typedefs.h"
#include "cooldown.h"

#include <stdint.h>

// python struct string "ffffffffbfbfbfbbbf"

GLOBALS_T stat;

int main (void)
// int loop (void)
{
  switch (stat.mode){
    case IDLING:
      break;
    case DESORBING:
      break;
    case THERMALISING:
      break;
    case COOLING:
      cooldown(&stat);
      break;
    case RUNNING:
      break;
  }
  // set_heaters(&stat);
  return 0;
}


// uint16_t calc_crc(GLOBALS_T status)
// {
//   uint16_t crc = 0;
//   for (uint8_t i=0; i<sizeof(GLOBALS_T); i++)
//   {
//     crc += (uint8_t)*(&status + i);
//   }
//   return crc;
// }


// void measure_temp(GLOBALS_T * status)
// {
//   float tempa = analogRead(blah);
//   status->temperatures.a = tempa;
// }


// void set_heaters(HEATERS_T * heaters)
// {
//   if (heaters->changed)
//   {
//     Serial.write("OP1 %d\n", heaters->a.enabled);
//     Serial.write("I1 %f\n", heaters->a.current);
//     Serial.write("OP2 %d\n", heaters->b.enabled);
//     Serial.write("I2 %f\n", heaters->b.current);
//     Serial.write("OP3 %d\n", heaters->c.enabled);
//     Serial.write("I3 %f\n", heaters->c.current);
//     Serial.write("OP4 %d\n", heaters->d.enabled);
//     Serial.write("I4 %f\n", heaters->d.current);
//   }
//   heaters->changed = 0;
// }




// void add_one (float * f)
// {
//   *f = *f + 1;
//   return *f;
// }


// float a = 10;

// add_one(&a);