unsigned long step_start;
unsigned long minutes = 60000;
unsigned long t;
int cooldown_step = 0;

void cooldown() {
  if (cooldown_step == 0) {
    print_info("Beginning cooldown sequence");
    cooldown_step = 1;
    step_start = millis();
  }

  t = millis() - step_start;

  if (cooldown_step == 1) {
    set_He4_pump(false);
    set_He3_pump(true);
    set_He4_switch(false);
    set_He3_switch(false);

    if (t > 5 * minutes) {
      cooldown_step = 2;
      step_start = millis();
    }
  } else if (cooldown_step == 2) {
    //turn on He4 switch
    set_He4_pump(false);
    set_He3_pump(true);
    set_He4_switch(true);
    set_He3_switch(false);

    if (He4Head_temperature < 2.0 && He3Head_temperature < 2.0) {
      cooldown_step = 3;
      step_start = millis();
    }
  } else if (cooldown_step == 3) {
    //turn off He3 pump
    set_He4_pump(false);
    set_He3_pump(false);
    set_He4_switch(true);
    set_He3_switch(false);

    if (t > 5 * minutes) {
      cooldown_step = 4;
      step_start = millis();
    }
  } else if (cooldown_step == 4) {
    //turn on He3 switch
    set_He4_pump(false);
    set_He3_pump(false);
    set_He4_switch(true);
    set_He3_switch(true);

    if (t > 30 * minutes) {
      cooldown_step = 5;
      step_start = millis();
    }
  } else if (cooldown_step == 5) {
    print_info("Cooldown complete");
    mode = AUTO;
    instructions = RUN;
    cooldown_step = 0;
  }
}

boolean printed_msg = false;
boolean startofcycle = true;

void cycle_pumps() {
  //  Need to raise He3 pump to 45-50K, and He4 pump to 50-60K
  //  Stay there for 30 minutes before cooling down again

  if (He3_switch_is_on() || He4_switch_is_on()) {
    if (!printed_msg) {
      print_info("A switch is still on, waiting for them to turn off");
      printed_msg = true;
    } else {
      printed_msg = false;
    }
    set_He3_switch(false);
    set_He4_switch(false);
    set_He3_pump(false);
    set_He4_pump(false);
  } else {
    printed_msg = false;
    set_He3_switch(false);
    set_He4_switch(false);
    set_He3_pump(true);
    set_He4_pump(true);
  }

  // If both pumps are warm, start a timer
  if (He4Pump_temperature > 50.0 && He3Pump_temperature > 45.0) {
    if (startofcycle) {
      step_start = millis();
      startofcycle = false;
    }
    t = millis() - step_start;
  } else {
    startofcycle = true;
  }

  // Once pumps have been warm for 30 mintures then cycling is complete
  if (t > 30 * minutes) {
    mode = AUTO;
    instructions = COOLDOWN;
    startofcycle = true;
  }
  delay(10);
}

void precool() {
  //  Need to raise He3 pump to 45-50K, and He4 pump to 50-60K, and keep them there while heads cool to 4 or 5K.
  //  Keep pumps warm for 20 min after heads cool beneath 5K

  if (He3_switch_is_on() || He4_switch_is_on()) {
    if (!printed_msg) {
      print_info("A switch is still on, waiting for them to turn off");
      printed_msg = true;
    } else {
      printed_msg = false;
    }
    set_He3_switch(false);
    set_He4_switch(false);
    set_He3_pump(false);
    set_He4_pump(false);
  } else {
    printed_msg = false;
    set_He3_switch(false);
    set_He4_switch(false);
    set_He3_pump(true);
    set_He4_pump(true);
  }

  // If both pumps are warm, start a timer
  if (He4Head_temperature < 5.0 && He3Head_temperature < 5.0) {
    if (startofcycle) {
      step_start = millis();
      startofcycle = false;
    }
    t = millis() - step_start;
  } else {
    startofcycle = true;
  }

  // Once heads have been below 5K for 20 mintures then precool is complete
  if (t > 20 * minutes) {
    mode = AUTO;
    instructions = COOLDOWN;
    startofcycle = true;
  }
  delay(10);
}

void run_cryocooler() {
  set_He4_pump(false);
  set_He3_pump(false);
  set_He4_switch(true);
  set_He3_switch(true);
  if (He3Head_temperature > 1.0) {
    print_info("He3 head above 1K, auto cycling pumps");
    mode = AUTO;
    instructions = CYCLEPUMPS;
  }
}

int voltage_step = 0;
void ramp_voltages() {
 
  if (voltage_step == 0) {
    POWERSUPPLY.write("I1 0.060\n");
    POWERSUPPLY.write("OP1 1\n");
    print_info("Beginning voltage sequence");
    voltage_step = 1;
    step_start = millis();
  }
  t = millis() - step_start;
  if (voltage_step == 1) {
     POWERSUPPLY.write("OP1 1\n");
     POWERSUPPLY.write("V1 7\n");
  } else if (voltage_step == 2) {
     POWERSUPPLY.write("OP1 1\n");
     POWERSUPPLY.write("V1 6.5\n");
  } else if (voltage_step == 3) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 6\n");
  } else if (voltage_step == 4) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 5.5\n");
  } else if (voltage_step == 5) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 5\n");
  } else if (voltage_step == 6) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 4.5\n");
  } else if (voltage_step == 7) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 4\n");
  } else if (voltage_step == 8) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 3.5\n");
  } else if (voltage_step == 9) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 3\n");
  } else if (voltage_step == 10) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 2.5\n");
  } else if (voltage_step == 11) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 2\n");
  } else if (voltage_step == 12) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 1.5\n");
  } else if (voltage_step == 13) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 1\n");
  } else if (voltage_step == 14) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 0.5\n");
  } else if (voltage_step == 15) {
     POWERSUPPLY.write("OP1 1\n");
    POWERSUPPLY.write("V1 0\n");
  }

  if (t > 15 * minutes) {
    voltage_step++;
    step_start = millis();
  }
}
