#define COMMUNICATION_H

// Define the Commands that can be sent and received
enum Commands {
  ACKNOWLEDGEMENT = 0,
  MAN = 1,
  AUTO = 2,
  VOLTAGES = 3,
  MODE = 4,
  TEMPERATURES = 5,
  INFO = 6,
  AUTOPILOT = 7,
};

typedef enum Commands Commands;

// Define the sequences that can be called in automatic mode
enum Sequence {
  COOLDOWN = 0,
  CYCLEPUMPS = 1,
  PRECOOL = 2,
  RUN = 3,
  OTHER = 4,
};

typedef enum Sequence Sequence;

void write_voltages(int V1, int V2, int V3, int V4, int V5, int V6, int V7)
{
  Serial.write(VOLTAGES);
  byte buf[2];
  buf[0] = V1 & 255;
  buf[1] = (V1 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  buf[0] = V2 & 255;
  buf[1] = (V2 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  buf[0] = V3 & 255;
  buf[1] = (V3 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  buf[0] = V4 & 255;
  buf[1] = (V4 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  buf[0] = V5 & 255;
  buf[1] = (V5 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  buf[0] = V6 & 255;
  buf[1] = (V6 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  buf[0] = V7 & 255;
  buf[1] = (V7 >> 8)  & 255;
  Serial.write(buf, sizeof(buf));
  Serial.write('\n');
}

float He3Head_temperature;
float He4Head_temperature;
float FilmBurner_temperature;
float He4Pump_temperature;
float He3Pump_temperature;
float He4Switch_temperature;
float He3Switch_temperature;

void request_temperatures() {
  Serial.write(TEMPERATURES);
  while (true) {
    if (Serial.read() == ACKNOWLEDGEMENT) {
      break;
    }
  }
  He3Head_temperature = Serial.readStringUntil('\n').toFloat();
  He4Head_temperature = Serial.readStringUntil('\n').toFloat();
  FilmBurner_temperature = Serial.readStringUntil('\n').toFloat();
  He4Pump_temperature = Serial.readStringUntil('\n').toFloat();
  He3Pump_temperature = Serial.readStringUntil('\n').toFloat();
  He4Switch_temperature = Serial.readStringUntil('\n').toFloat();
  He3Switch_temperature = Serial.readStringUntil('\n').toFloat();
  numeric_message(He3Head_temperature);
}

void print_info(String message) {
  Serial.write(INFO);
  Serial.print(message);
  Serial.write('\n');
}

boolean PC_present;
Commands mode;
byte instructions;
boolean autopilot_enabled = false;
float target_temperature;
void get_PC_commands() {
  Serial.write(MODE);
  delay(100);
  if (Serial.available() > 0) {
    PC_present = true;
    mode = (Commands) Serial.read();
    instructions = Serial.read();
    if (instructions == OTHER) {
      target_temperature = Serial.readStringUntil('\n').toFloat();
    }
    if (mode == AUTOPILOT) {
      print_info("Entering autopilot mode");
      autopilot_enabled = true;
      mode = AUTO;
    }
  } else {
    PC_present = false;
  }
}

byte instructions_temp;
void check_if_not_autopilot() {
  Serial.write(MODE);
  delay(100);
  if (Serial.available() > 0) {
    PC_present = true;
    mode = (Commands) Serial.read();
    instructions_temp = Serial.read();
    if (instructions_temp == OTHER) {
      target_temperature = Serial.readStringUntil('\n').toFloat();
    }
    if (mode == AUTOPILOT) {
      mode = AUTO;
    } else {
      print_info("Exiting autopilot mode");
      autopilot_enabled = false;
      instructions = instructions_temp;
    }
  } else {
    PC_present = false;
  }
}
