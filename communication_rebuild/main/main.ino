// set this to the hardware serial port you wish to use
#define POWERSUPPLY Serial1
#define SEGMENT Serial2

int controller_state = B00000000;

#include "communication.h"
#include <PID_v1.h>
#include "PID_control.h"

// define teensy pins
const int ledPin = 13;
const int voltAPin = 16;
const int voltBPin = 15;
const int voltCPin = 21;
const int voltDPin = 20;
const int voltEPin = 19;
const int voltFPin = 18;
const int voltGPin = 17;
const int ACDrivePin = 23;
const int He3switchPin = 6;
const int He4switchPin = 5;

// set up parameters used to keep track of lock in
int ACState = LOW;
int multiplier = 1;
int temperature_counter = 0;
const int measures_per_cycle = 40; //The number of measurements per cycle, where each takes ~1.25 ms

#define ONESEC (1000UL)
unsigned long rolltime = millis() + ONESEC;

void setup() {
  //put teensy ADC into high resolution mode
  analogReadRes(13);

  Serial.begin(115200); // open the serial port at 115200 bps:
  //open serial port to power supply
  POWERSUPPLY.begin(19200, SERIAL_8N1);
  SEGMENT.begin(9600, SERIAL_8N1);

  // prepare all used teensy pins
  pinMode(ledPin, OUTPUT);
  pinMode(voltAPin, INPUT);
  pinMode(voltBPin, INPUT);
  pinMode(voltCPin, INPUT);
  pinMode(voltDPin, INPUT);
  pinMode(voltEPin, INPUT);
  pinMode(voltFPin, INPUT);
  pinMode(voltGPin, INPUT);
  pinMode(ACDrivePin, OUTPUT);
  pinMode(He3switchPin, OUTPUT);
  digitalWrite(He3switchPin, LOW);
  pinMode(He4switchPin, OUTPUT);
  digitalWrite(He4switchPin, LOW);

  He3pump_PID.SetMode(AUTOMATIC);
  He4pump_PID.SetMode(AUTOMATIC);
  He3head_PID.SetMode(AUTOMATIC);
}


void loop() {
  if (PC_present) {
    measure_voltages();
  }

  if ((long)(millis() - rolltime) >= 0) {

    // once every one second, execute this sequence
    if (autopilot_enabled){
      check_if_not_autopilot();
    } else {
      get_PC_commands();
    }
    if (PC_present) {
      request_temperatures();
    }

    if (mode == MAN) {
      execute_manual_instructions(instructions);
    } else if (mode == AUTO) {
      switch (instructions) {
        case COOLDOWN:
          cooldown();
          break;
        case CYCLEPUMPS:
          cycle_pumps();
          break;
        case PRECOOL:
          precool();
          break;
        case RUN:
          run_cryocooler();
          break;
        case OTHER:
          control_He3_head(target_temperature);
          He3_switch(true);
          He4_switch(true);
          break;
      }
    }
    if (instructions!=OTHER){
      update_controller(controller_state);
    }

    // reset to always the same current polarity
    ACState = LOW;
    digitalWrite(ACDrivePin, ACState);
    multiplier = 1;
    temperature_counter = 0;

    rolltime += ONESEC;
    delay(1);
  }
}
