#include <math.h>
#define APOSTROPHE  5
#define COLON       4
#define DECIMAL4    3
#define DECIMAL3    2
#define DECIMAL2    1
#define DECIMAL1    0

void single_message(String input) {
  if (input.length() > 4) {
    SEGMENT.write(0x76);
    SEGMENT.print("Err1");
  } else {
    SEGMENT.write(0x76);
    SEGMENT.print(input);
    SEGMENT.write(0x77);
    SEGMENT.write(0b00000100);
  }
}

void numeric_message(float input) {
  int exponent = floor(log10(input));
  float coeff = input / pow(10, exponent);

  if (exponent <= 0) {
    String mesg = round(input * pow(10, 3)); //round input to 3 decimal places
    SEGMENT.write(0x76); //clear SEGMENT
    SEGMENT.print("0000"); //SEGMENT 0000
    SEGMENT.write(0x79); //change cursor position
    SEGMENT.write(-exponent); //put curson in the position of the first sig fig
    SEGMENT.print(mesg); //write the input
    SEGMENT.write(0x77); //set decimal point
    SEGMENT.write(0b00000001); //light up decimal point so it's of the form "x.xxx"
  } else {
    String mesg = round(coeff * pow(10, 3)); //round input to four sig figs
    SEGMENT.write(0x76); //clear SEGMENT
    SEGMENT.print(mesg); //SEGMENT input numbers
    SEGMENT.write(0x77); //set decimal point
    SEGMENT.write(0b00000001 << exponent); //put decimal point in the write place
  }

}
