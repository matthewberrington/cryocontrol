#!/usr/bin/python
import os
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import time
import numpy as np
import datetime

class Superplot():
    def __init__(self,name):
        self.app = QtGui.QApplication([])
        
        self.mw = QtGui.QMainWindow()
        self.mw.setWindowTitle('pyqtgraph example: PlotWidget')
        self.mw.resize(800,500)
        self.cw = QtGui.QWidget()
        self.mw.setCentralWidget(self.cw)
        l = QtGui.QHBoxLayout()
        self.cw.setLayout(l)

        p1 = pg.PlotWidget(name='Plot1')  ## giving the plots names allows us to link their axes together
        p1.plot()
        l.addWidget(p1)
        p2 = pg.PlotWidget(name='Plot2')
        p2.plot()
        l.addWidget(p2)
        self.mw.show()

        p1.setDownsampling(mode='peak')
        p2.setDownsampling(mode='peak')
        p1.setClipToView(True)
        p2.setClipToView(True)
        p1.setLogMode(y = True)
        p2.setLogMode(y = True)
        p1.addLegend()
        p2.addLegend()

        p1.showGrid(x = True, y = True, alpha = 0.4)
        p2.showGrid(x = True, y = True, alpha = 0.4)

        p1.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
        p1.setLabel('bottom', "Time", units='hours', **{'color': '#FFF', 'font-size': '12pt'})
        p2.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
        p2.setLabel('bottom', "Time", units='minutes', **{'color': '#FFF', 'font-size': '12pt'})

        self.curve1A = p1.plot(pen=(250,129,116), name="3He Head")
        self.curve1B = p1.plot(pen=(129,177,210), name="4He Head")
        self.curve1C = p1.plot(pen=(253,180,98), name="Film Burner")
        self.curve1D = p1.plot(pen=(179,222,105), name="4He Pump")
        self.curve1E = p1.plot(pen=(188,130,189), name="3He Pump")
        self.curve1F = p1.plot(pen=(204,235,196), name="4He Switch")
        self.curve1G = p1.plot(pen=(255,237,111), name="3He Switch")

    def temp(self):
        x = np.arange(1000)
        self.curve1A.setData(x,np.random.normal(size=(1000)))
        self.curve1B.setData(x,np.random.normal(size=(1000)))
        self.curve1C.setData(x,np.random.normal(size=(1000)))
        self.curve1D.setData(x,np.random.normal(size=(1000)))
        self.curve1E.setData(x,np.random.normal(size=(1000)))
        self.curve1F.setData(x,np.random.normal(size=(1000)))
        self.curve1G.setData(x,np.random.normal(size=(1000)))
        self.app.processEvents()

        
            



app = QtGui.QApplication([])
        
mw = QtGui.QMainWindow()
mw.setWindowTitle('pyqtgraph example: PlotWidget')
mw.resize(800,500)
cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QHBoxLayout()
cw.setLayout(l)

p1 = pg.PlotWidget(name='Plot1')  ## giving the plots names allows us to link their axes together
p1.plot()
l.addWidget(p1)
p2 = pg.PlotWidget(name='Plot2')
p2.plot()
l.addWidget(p2)
mw.show()

p1.setDownsampling(mode='peak')
p2.setDownsampling(mode='peak')
p1.setClipToView(True)
p2.setClipToView(True)
p1.setLogMode(y = True)
p2.setLogMode(y = True)
p1.addLegend()
p2.addLegend()

p1.showGrid(x = True, y = True, alpha = 0.4)
p2.showGrid(x = True, y = True, alpha = 0.4)

p1.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
p1.setLabel('bottom', "Time", units='hours', **{'color': '#FFF', 'font-size': '12pt'})
p2.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
p2.setLabel('bottom', "Time", units='minutes', **{'color': '#FFF', 'font-size': '12pt'})

curve1A = p1.plot(pen=(250,129,116), name="3He Head")
curve1B = p1.plot(pen=(129,177,210), name="4He Head")
curve1C = p1.plot(pen=(253,180,98), name="Film Burner")
curve1D = p1.plot(pen=(179,222,105), name="4He Pump")
curve1E = p1.plot(pen=(188,130,189), name="3He Pump")
curve1F = p1.plot(pen=(204,235,196), name="4He Switch")
curve1G = p1.plot(pen=(255,237,111), name="3He Switch")

if __name__ == '__main__':
    
    def parsetime(v): 
        return np.datetime64(
            datetime.datetime.strptime(v.decode(), '%H:%M:%S')
        )

    time_modified = os.stat('test.log')[8]

    data = np.genfromtxt(
        "test.log", 
        delimiter=",",
        usecols=[1,2,3,4,5,6,7])

    t = np.genfromtxt(
        "test.log", 
        delimiter=",",
        usecols=0,
        converters={0: parsetime},)
    
    t = np.array(t - t[-1],dtype=float)/3.6e9
    
    curve1A.setData(t,data[:,0])
    curve1B.setData(t,data[:,1])
    curve1C.setData(t,data[:,2])
    curve1D.setData(t,data[:,3])
    curve1E.setData(t,data[:,4])
    curve1F.setData(t,data[:,5])
    curve1G.setData(t,data[:,6])
    pg.QtGui.QApplication.processEvents()


# s = Superplot('asd')
# while True:
#     t0 = time.time()
#     s.temp()
#     print(time.time()-t0)
