import time
import numpy as np

import logging
from logging.handlers import TimedRotatingFileHandler

#Set up all the logging stuff
#temp_logger = setup_logger('temperature', 'logs/temperature.log')
temp_logger = logging.getLogger('test')
temp_logger.setLevel(logging.INFO)
#    handler = TimedRotatingFileHandler(filename, when="midnight")    
handler = TimedRotatingFileHandler('test.log', when='S', interval=15)    
formatter = logging.Formatter(fmt='%(asctime)s,%(message)s', datefmt = '%H:%M:%S')
handler.setFormatter(formatter)
temp_logger.addHandler(handler)


from pyqtgraph.Qt import QtGui, QtCore
import pyqtgraph as pg
# from multiprocessing import Process, Queue
import multiprocessing


class Tester():
    def start(self):
        self.q = multiprocessing.Queue()
        self.p = multiprocessing.Process(target=self.run)
        self.p.start()
        return self.q

    def _update(self):
        while not self.q.empty():
            self.q.get()

    def run(self):
        app = QtGui.QApplication([])

        timer = QtCore.QTimer()
        timer.timeout.connect(self._update)
        timer.start(50)

        app.exec_()
        
def main():    
    multiprocessing.set_start_method('spawn')
    s = Tester()
    q = s.start()
    
    t0 = time.time()
    t = 0
    while True:    
        s = np.sin(0.05*2 * np.pi * t)
        # q.put((time.time()-t0,s*1+8,s*2+8,s*3+8,s*4+8,s*5+8,s*6+8,s*7+8))
        time.sleep(0.01)
        t+=0.1    
        if t%10 <0.1:
            temp_logger.info("Hello")
        
if __name__ == '__main__':
    main()