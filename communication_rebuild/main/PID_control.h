#define PID_CONTROL_H


//Define variables we'll be connecting to our PIDs
double He3pump_input, He3pump_output, He3pump_setpoint;
double He4pump_input, He4pump_output, He4pump_setpoint;

double He3head_input, He3head_output, He3head_setpoint;

//Specify the links and initial tuning parameters for PIDs
//Reversed as increase current = increase temperature = decrease resistance
PID He3pump_PID(&He3pump_input, &He3pump_output, &He3pump_setpoint, 0.004, 0.0003, 0.0, DIRECT);
PID He4pump_PID(&He4pump_input, &He4pump_output, &He4pump_setpoint, 0.010, 0.0005, 0.0, DIRECT);

PID He3head_PID(&He3head_input, &He3head_output, &He3head_setpoint, 30.0, 3.0, 0.0, DIRECT);
//50/10 gain still a litle high. When starting at 300mK it went up to 570mK, then down to 465mK, then settled on 500mK.

//50/5 gain still a litle high. When starting at 300mK it went up to 506mK, then down to 464mK, then settled on 500mK.
//40/5 gain still a litle high. When starting at 300mK it went up to 506mK, then down to 464mK, then settled on 500mK.


void He3_PID() {
  He3pump_PID.Compute();
  String He3current_command = "I1 " + String(He3pump_output, 3).substring(0, 5) + "\n"; //Take first four character of the output. This should be "0.xxx" 
  POWERSUPPLY.print("V1 16\n"); //set limit voltage supply to 24V,  incase the PID says outputs something stupid
  POWERSUPPLY.print(He3current_command); //this used to be POWERSUPPLY.write, but I think this works to
  POWERSUPPLY.write("OP1 1\n");
}

void He4_PID() {
  He4pump_PID.Compute();
  String He4current_command = "I2 " + String(He4pump_output, 3).substring(0, 5) + "\n"; //Take first four character of the output. This should be "0.xxx"
  POWERSUPPLY.print("V2 28\n"); //set limit voltage supply to 25V, incase the PID says outputs something stupid
  POWERSUPPLY.print(He4current_command); //this used to be POWERSUPPLY.write, but I think this works to
  POWERSUPPLY.write("OP2 1\n");
}

void Temperature_PID() {
  He3head_PID.Compute();
  String He3voltage_command = "V1 " + String(He3head_output, 3).substring(0, 5) + "\n"; //Take first four character of the output. This should be "0.xxx"
  POWERSUPPLY.print("I1 0.050\n"); //set limit current to the 30mA, which should be enough to have head above 1K
  POWERSUPPLY.print(He3voltage_command); //this used to be POWERSUPPLY.write, but I think this works to
  POWERSUPPLY.write("OP1 1\n");
}
