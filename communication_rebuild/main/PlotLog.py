#!/usr/bin/python
import os
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import time
import numpy as np
import datetime
import matplotlib.pyplot as plt

app = QtGui.QApplication([])
        
mw = QtGui.QMainWindow()
mw.setWindowTitle('pyqtgraph example: PlotWidget')
mw.resize(800,500)
cw = QtGui.QWidget()
mw.setCentralWidget(cw)
l = QtGui.QHBoxLayout()
cw.setLayout(l)

p1 = pg.PlotWidget(name='Plot1')  ## giving the plots names allows us to link their axes together
p1.plot()
l.addWidget(p1)
p2 = pg.PlotWidget(name='Plot2')
p2.plot()
l.addWidget(p2)
mw.show()

p1.setDownsampling(auto=True,mode='peak')
p2.setDownsampling(mode='peak')
p1.setClipToView(True)
p2.setClipToView(True)
p1.setLogMode(y = True)
p2.setLogMode(y = True)
p1.addLegend()
p2.addLegend()

p1.showGrid(x = True, y = True, alpha = 0.4)
p2.showGrid(x = True, y = True, alpha = 0.4)

p1.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
p1.setLabel('bottom', "Time", units='hours', **{'color': '#FFF', 'font-size': '12pt'})
p2.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
p2.setLabel('bottom', "Time", units='minutes', **{'color': '#FFF', 'font-size': '12pt'})

curve1A = p1.plot(pen=(250,129,116), name="3He Head")
curve1B = p1.plot(pen=(129,177,210), name="4He Head")
curve1C = p1.plot(pen=(253,180,98), name="Film Burner")
curve1D = p1.plot(pen=(179,222,105), name="4He Pump")
curve1E = p1.plot(pen=(188,130,189), name="3He Pump")
curve1F = p1.plot(pen=(204,235,196), name="4He Switch")
curve1G = p1.plot(pen=(255,237,111), name="3He Switch")

curve2A = p2.plot(pen=(250,129,116), name="3He Head")
curve2B = p2.plot(pen=(129,177,210), name="4He Head")
curve2C = p2.plot(pen=(253,180,98), name="Film Burner")
curve2D = p2.plot(pen=(179,222,105), name="4He Pump")
curve2E = p2.plot(pen=(188,130,189), name="3He Pump")
curve2F = p2.plot(pen=(204,235,196), name="4He Switch")
curve2G = p2.plot(pen=(255,237,111), name="3He Switch")

def parsetime(v): 
    return np.datetime64(
        datetime.datetime.strptime(v.decode(), '%Y-%m-%d %H:%M:%S')
    )

def get_data(filename):
   
    t0 = time.time()
    data = np.genfromtxt(
    filename, 
    delimiter=",",
    converters={0: parsetime},)
    # print(time.time()-t0)
    t = np.array(list(zip(*data))[0])
    
    temperatures = np.transpose(np.array(list(zip(*data))[1:]))
    # print(time.time()-t0)
    return t, temperatures


def get_last_24hours():
    yesterday = (datetime.datetime.now() - datetime.timedelta(days=1))
    yesterday_suffix = yesterday.strftime(".%Y-%m-%d")
    t_today, temperatures_today = get_data("logs/temperature.log")
    t_yesterday, temperatures_yesterday = get_data("logs/temperature.log"+yesterday_suffix)

    t = np.append(t_yesterday,t_today)
    temperatures = np.append(temperatures_yesterday,temperatures_today,axis=0)
    
    picker_last24hours = t>yesterday
    t_24hours = t[picker_last24hours]
    temperatures_24hours = temperatures[picker_last24hours]
    t_fivemin = np.array(t_24hours - t_24hours[-1],dtype=float)/3.6e9
    
    return t_fivemin, temperatures_24hours
        
    

if __name__ == '__main__':
    while True:
        yesterday = (datetime.datetime.now() - datetime.timedelta(days=1))
        five_min_ago = (datetime.datetime.now() - datetime.timedelta(minutes=5))
        six_hour_ago = (datetime.datetime.now() - datetime.timedelta(hours=6))

        t, temperatures = get_data("logs/temperature.log")
        
        picker_last5mins = t>five_min_ago
        t_fivemin = t[picker_last5mins]
        temperatures_fivemin = temperatures[picker_last5mins]
        t_fivemin = np.array(t_fivemin - t_fivemin[-1],dtype=float)/6e7
        
        
        picker_last6hours = t>six_hour_ago
        t_sixhour = t[picker_last6hours]
        temperatures_sixhour = temperatures[picker_last6hours]
        t_sixhour = np.array(t_sixhour - t_sixhour[-1],dtype=float)/3.6e9
        
        curve1A.setData(t_sixhour,temperatures_sixhour[:,0])
        curve1B.setData(t_sixhour,temperatures_sixhour[:,1])
        curve1C.setData(t_sixhour,temperatures_sixhour[:,2])
        curve1D.setData(t_sixhour,temperatures_sixhour[:,3])
        curve1E.setData(t_sixhour,temperatures_sixhour[:,4])
        curve1F.setData(t_sixhour,temperatures_sixhour[:,5])
        curve1G.setData(t_sixhour,temperatures_sixhour[:,6])
        
        curve2A.setData(t_fivemin,temperatures_fivemin[:,0])
        curve2B.setData(t_fivemin,temperatures_fivemin[:,1])
        curve2C.setData(t_fivemin,temperatures_fivemin[:,2])
        curve2D.setData(t_fivemin,temperatures_fivemin[:,3])
        curve2E.setData(t_fivemin,temperatures_fivemin[:,4])
        curve2F.setData(t_fivemin,temperatures_fivemin[:,5])
        curve2G.setData(t_fivemin,temperatures_fivemin[:,6])
        pg.QtGui.QApplication.processEvents()