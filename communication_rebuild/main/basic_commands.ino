void He4_switch(boolean status) {
  if (status == true) {
    POWERSUPPLY.write("OP3 1\n");
    digitalWrite(He4switchPin, HIGH);
  } else {
    digitalWrite(He4switchPin, LOW);
  }
}

void He3_switch(boolean status) {
  if (status == true) {
    POWERSUPPLY.write("OP3 1\n");
    digitalWrite(He3switchPin, HIGH);
  } else {
    digitalWrite(He3switchPin, LOW);
  }
}

void He3_rapid_heat() {
  POWERSUPPLY.write("V1 16\n");
  POWERSUPPLY.write("I1 0.060\n"); //limit current to the upper current the manual suggests
}

void He4_rapid_heat() {
  POWERSUPPLY.write("V2 28\n");
  POWERSUPPLY.write("I2 0.130\n"); //limit current to the upper current the manual suggests
}

void He4_pump(boolean status) {
  if (status) {
    POWERSUPPLY.write("OP2 1\n");
    if (He4Pump_temperature < 40.0) {
      He4_rapid_heat();
    } else {
      He4pump_setpoint = 55.0;
      He4pump_input = He4Pump_temperature;
      He4_PID();
    }
  } else {
    //  turn off He4 pump
    POWERSUPPLY.write("V2 0\n");
    POWERSUPPLY.write("I2 0\n");
    POWERSUPPLY.write("OP2 0\n");
  }
}

void He3_pump(boolean status) {
  if (status == true) {
    POWERSUPPLY.write("OP1 1\n");
    if (He3Pump_temperature < 35.0) {
      He3_rapid_heat();
    } else {
      He3pump_setpoint = 48.0;
      He3pump_input = He3Pump_temperature;
      He3_PID();
    }
  } else {
    //  turn off He3 pump
    POWERSUPPLY.write("V1 0\n");
    POWERSUPPLY.write("I1 0\n");
    POWERSUPPLY.write("OP1 0\n");
  }
}

boolean He3_switch_is_on() {
  if (He3Switch_temperature > 12.0) {
    return true;
  } else {
    return false;
  }
}

boolean He4_switch_is_on() {
  if (He4Switch_temperature > 12.0) {
    return true;
  } else {
    return false;
  }
}

boolean He3_pump_is_on() {
  // for some reason the power supply spits back all past instructions when you first use a ?
  // so I query voltage once and throw the response away
  //  return true;
  POWERSUPPLY.write("V1?\n");
  delay(1);
  POWERSUPPLY.readString();
  delay(1);
  //now for the actual request
  POWERSUPPLY.write("V1?\n");
  delay(1);
  String resp = POWERSUPPLY.readString();
  float resp_float = resp.remove(0, 7).toFloat(); //chop off first seven characters to get just the number
  boolean is_on;
  if (resp_float == 0.00) {
    is_on = false;
  } else {
    is_on = true;
  }
  return is_on;
}


boolean He4_pump_is_on() {
  //  return true;
  // for some reason the power supply spits back all past instructions when you first use a ?
  // so I query voltage once and throw the response away
  POWERSUPPLY.write("V2?\n");
  delay(1);
  POWERSUPPLY.readString(); //throw away
  delay(1);
  //now for the actual request
  POWERSUPPLY.write("V2?\n");
  delay(1);
  String resp = POWERSUPPLY.readString();
  float resp_float = resp.remove(0, 7).toFloat(); //chop off first seven characters to get just the number
  boolean is_on;
  if (resp_float == 0.00) {
    is_on = false;
  } else {
    is_on = true;
  }
  return is_on;
}

void flip_polarity() {
  // if the current is normal then reversed it and vice-versa:
  if (ACState == LOW) {
    ACState = HIGH;
    multiplier = -1;
  } else {
    ACState = LOW;
    multiplier = 1;
  }
  // implement new current direction
  digitalWrite(ACDrivePin, ACState);
}

int voltA_value;
int voltB_value;
int voltC_value;
int voltD_value;
int voltE_value;
int voltF_value;
int voltG_value;


void measure_voltages() {
  voltA_value = multiplier * analogRead(voltAPin);
  voltB_value = multiplier * analogRead(voltBPin);
  voltC_value = analogRead(voltCPin);
  voltD_value = analogRead(voltDPin);
  voltE_value = analogRead(voltEPin);
  voltF_value = analogRead(voltFPin);
  voltG_value = analogRead(voltGPin);
  write_voltages(voltA_value, voltB_value, voltC_value, voltD_value, voltE_value, voltF_value, voltG_value);

//  current_V = voltA_value*3300.0/8191.0;
//  current_R = 4.438715402514645*current_V - 65.45195638779182;
//  current_T = R_to_T(current_R)
  
  temperature_counter++;

  //if half the cycle time has passed, flip current polarity
  if (temperature_counter >= measures_per_cycle / 2) {
    flip_polarity();
    temperature_counter = 0;
  }
  delay(1);
  
}

void control_He3_head(double setpoint) {
  He3head_setpoint = setpoint;
  He3head_input = He3Head_temperature;
  Temperature_PID();
  }
