void set_He4_pump(boolean val) {
  //if state of controller changes, communicate the change
  if (bitRead(controller_state, 0) != val){
    if (val){
      print_info("Turning on He4 pump");
    } else {
      print_info("Turning off He4 pump");
    }
  }
  
  bitWrite(controller_state, 0, val);
}

void set_He3_pump(boolean val) {
    //if state of controller changes, communicate the change
  if (bitRead(controller_state, 1) != val){
    if (val){
      print_info("Turning on He3 pump");
    } else {
      print_info("Turning off He3 pump");
    }
  }
  
  bitWrite(controller_state, 1, val);
}

void set_He4_switch(boolean val) {
    //if state of controller changes, communicate the change
  if (bitRead(controller_state, 2) != val){
    if (val){
      print_info("Turning on He4 switch");
    } else {
      print_info("Turning off He4 switch");
    }
  }
  
  bitWrite(controller_state, 2, val);
}

void set_He3_switch(boolean val) {
    //if state of controller changes, communicate the change
  if (bitRead(controller_state, 3) != val){
    if (val){
      print_info("Turning on He3 switch");
    } else {
      print_info("Turning off He3 switch");
    }
  }
  
  bitWrite(controller_state, 3, val);
}
void execute_manual_instructions(byte instructions) {
  set_He4_pump(bitRead(instructions, 0));
  set_He3_pump(bitRead(instructions, 1));
  set_He4_switch(bitRead(instructions, 2));
  set_He3_switch(bitRead(instructions, 3));
}


void update_controller(byte state) {
  He4_pump(bitRead(state, 0));
  He3_pump(bitRead(state, 1));
  He4_switch(bitRead(state, 2));
  He3_switch(bitRead(state, 3));
}
