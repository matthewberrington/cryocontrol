import logging
import os
from logging.handlers import TimedRotatingFileHandler

def setup_logger(name, filename, level=logging.INFO):
    """Function setup as many loggers as you want"""
    logger = logging.getLogger(name)
    logger.setLevel(level)
#    handler = TimedRotatingFileHandler(filename, when="midnight")    
    handler = TimedRotatingFileHandler(filename, when='M', interval=5)    
    formatter = logging.Formatter(fmt='%(asctime)s,%(message)s', datefmt = '%H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger

if not os.path.exists("logs"):
    os.makedirs("logs")

#Set up all the logging stuff
temp_logger = setup_logger('temperature', 'logs/temperature.log')
comm_logger = setup_logger('communication', 'logs/communication.log')
volt_logger = setup_logger('voltage', 'logs/voltage.log')