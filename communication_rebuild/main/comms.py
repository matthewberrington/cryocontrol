
import numpy as np
#import MultiLogger
import time
import csv
from scipy.interpolate import interp1d
import threading
import Thermometers
# import LivePlot

import serial
import struct
from enum import Enum


#########################
import logging
import os
from logging.handlers import TimedRotatingFileHandler

#def setup_logger(name, filename, level=logging.INFO):
#    """Function setup as many loggers as you want"""
#    logger = logging.getLogger(name)
#    logger.setLevel(level)
##    handler = TimedRotatingFileHandler(filename, when="midnight")    
#    handler = TimedRotatingFileHandler(filename, when='M', interval=5)    
#    formatter = logging.Formatter(fmt='%(asctime)s,%(message)s', datefmt = '%H:%M:%S')
#    handler.setFormatter(formatter)
#    logger.addHandler(handler)
#
#    return logger

if not os.path.exists("logs"):
    os.makedirs("logs")

#Set up all the logging stuff
#temp_logger = setup_logger('temperature', 'logs/temperature.log')
temp_logger = logging.getLogger('temperature') 
temp_logger.setLevel(logging.INFO)
#    handler = TimedRotatingFileHandler(filename, when="midnight")    
handler = TimedRotatingFileHandler('logs/temperature.log', when='midnight')    
formatter = logging.Formatter(fmt='%(asctime)s,%(message)s', datefmt = '%Y-%m-%d %H:%M:%S')
handler.setFormatter(formatter)
temp_logger.addHandler(handler)

# comm_logger = setup_logger('communication', 'logs/communication.log')
#volt_logger = setup_logger('voltage', 'logs/voltage.log')
#########################

def load_diode_calibration(filename='diode_calib.csv'):
    T0 = np.array([])
    V0 = np.array([])
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            V0 = np.append(V0,float(row[1]))
            T0 = np.append(T0,float(row[0]))
    return interp1d(V0, T0)

def load_thermistor_calibration(filename='resistor_calib.csv'):
    T0 = np.array([])
    R0 = np.array([])
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            R0 = np.append(R0,float(row[1]))
            T0 = np.append(T0,float(row[0]))
    return interp1d(R0, T0)

# read in calibration from cryocooler manufacturers
diode_cal = load_diode_calibration()
thermistor_cal = load_thermistor_calibration()

class volt_calculator():
    def __init__(self):
        self.reset_memory()
        # define thermometers
        self.thermometerA = Thermometers.Thermistor(mVtoR_He3,mVtoRerr_He3,thermistor_cal)
        self.thermometerB = Thermometers.Thermistor(mVtoR_He4,mVtoRerr_He4,thermistor_cal)
        self.thermometerC = Thermometers.DiodeThermometer(diode_cal)
        self.thermometerD = Thermometers.DiodeThermometer(diode_cal)
        self.thermometerE = Thermometers.DiodeThermometer(diode_cal)
        self.thermometerF = Thermometers.DiodeThermometer(diode_cal)
        self.thermometerG = Thermometers.DiodeThermometer(diode_cal)
    
    def reset_memory(self):
        self.VA_list = []
        self.VB_list = []
        self.VC_list = []
        self.VD_list = []
        self.VE_list = []
        self.VF_list = []
        self.VG_list = []
    
    def add_new_measurement(self,voltages):
        V_A, V_B, V_C, V_D, V_E, V_F, V_G = voltages
#        print(voltages)
        self.VA_list.append(V_A*3300.0/8191.0)
        self.VB_list.append(V_B*3300.0/8191.0)
        self.VC_list.append(V_C*3300.0/8191.0)
        self.VD_list.append(V_D*3300.0/8191.0)
        self.VE_list.append(V_E*3300.0/8191.0)
        self.VF_list.append(V_F*3300.0/8191.0)
        self.VG_list.append(V_G*3300.0/8191.0)
    
    def calculate_voltages(self):
        lockin_cycles = len(self.VA_list) // 40
        num_valid_datapoints = lockin_cycles*40
        if lockin_cycles == 0:
            return (0,0), (0,0), (0,0), (0,0), (0,0), (0,0), (0,0)
        VA = np.abs(np.mean(self.VA_list[:num_valid_datapoints]))*2
        VB = np.abs(np.mean(self.VB_list[:num_valid_datapoints]))*2
        VC = np.mean(self.VC_list[:num_valid_datapoints])
        VD = np.mean(self.VD_list[:num_valid_datapoints])
        VE = np.mean(self.VE_list[:num_valid_datapoints])
        VF = np.mean(self.VF_list[:num_valid_datapoints])
        VG = np.mean(self.VG_list[:num_valid_datapoints])
        
        sigmaVA = np.std(self.VA_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)
        sigmaVB = np.std(self.VB_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)
        sigmaVC = np.std(self.VC_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)
        sigmaVD = np.std(self.VD_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)
        sigmaVE = np.std(self.VE_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)
        sigmaVF = np.std(self.VF_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)
        sigmaVG = np.std(self.VG_list[:num_valid_datapoints])/np.sqrt(num_valid_datapoints)\
        
        return (VA,sigmaVA), (VB,sigmaVB), (VC,sigmaVC), (VD,sigmaVD), (VE,sigmaVE), (VF,sigmaVF), (VG,sigmaVG)
    
    def calculate_temperature(self,voltages):
        (VA,sigmaVA), (VB,sigmaVB), (VC,sigmaVC), (VD,sigmaVD), (VE,sigmaVE), (VF,sigmaVF), (VG,sigmaVG) = voltages
        self.thermometerA.set_voltage((VA,sigmaVA))
        self.thermometerB.set_voltage((VB,sigmaVB))
        self.thermometerC.set_voltage((VC,sigmaVC))
        self.thermometerD.set_voltage((VD,sigmaVD))
        self.thermometerE.set_voltage((VE,sigmaVE))
        self.thermometerF.set_voltage((VF,sigmaVF))
        self.thermometerG.set_voltage((VG,sigmaVG))
        
        return self.thermometerA.T, self.thermometerB.T, self.thermometerC.T, self.thermometerD.T, self.thermometerE.T, self.thermometerF.T, self.thermometerG.T

def user_input():
    global mode 
    global instructions
    global target_temperature
    while True:
        
        time.sleep(1)
        user_input = input()

        try:
            mode_temp, instructions_temp = user_input.split(",")
        except:
            try:
                mode_temp, instructions_temp, target_temperature = user_input.split(",")
                print(target_temperature)
                print(type(target_temperature))
            except:
                print("Input error: needs two instructions separated by one comma")
                continue

        if mode_temp not in ['MAN','AUTO','AUTOPILOT']:
            print("Input error: first entry needs to be MAN, AUTO or AUTOPILOT")
            continue


        
        mode_temp = Command._member_map_[mode_temp]
    
        if mode_temp == Command.MAN:
            try:
                instructions_temp = int(instructions_temp,2)
            except:
                print("Input error: manual instructions must be in binary") 
                continue
        elif mode_temp == Command.AUTO:
            # if instructions in Sequence._member_names_:
            try:
                instructions_temp = Sequence._member_map_[instructions_temp].value
            except:
                print("Input error: instruction is not a valid sequence")
                continue
        elif mode_temp == Command.AUTOPILOT:
            # if instructions in Sequence._member_names_:
            try:
                instructions_temp = Sequence._member_map_[instructions_temp].value
            except:
                print("Input error: instruction is not a valid sequence")
                continue
        mode = mode_temp
        instructions = instructions_temp
        
def mVtoR_He3(V):
    return 4.438715402514645*V - 65.45195638779182  #from calibration 07/05/19

def mVtoR_He4(V):
    return 1.3387753721881892*V - 124.47354417022443 #from calibration 07/05/19

def mVtoRerr_He3(V, sigma_V):
    return V*0.030869340521269124 + 29.883740251127055 + 4.438715402514645*sigma_V #from calibration 07/05/19

def mVtoRerr_He4(V, sigma_V):
    return V*0.004038442690370698 + 7.7866030117882294 + 1.3387753721881892*sigma_V #from calibration 07/05/19



class Command(Enum):
    """
    Pre-defined Commands
    """

    ACKNOWLEDGEMENT = 0
    MAN = 1
    AUTO = 2
    VOLTAGES = 3
    MODE = 4
    TEMPERATURES = 5
    INFO = 6
    AUTOPILOT = 7

class Sequence(Enum):
    """ 
    Pre-defined Sequence
    """

    COOLDOWN = 0
    CYCLEPUMPS = 1
    PRECOOL = 2
    RUN = 3
    OTHER = 4


def write_command(f, command):
    """
    :param f: file handler or serial file
    :param command: (Command Enum Object)
    """
    write_i8(f, command.value)

def write_i8(f, value):
    """
    :param f: file handler or serial file
    :param value: (int8_t)
    """
    if -128 <= value <= 127:
        f.write(struct.pack('<b', value))
    else:
        print("Value error:{}".format(value))

def write_temperatures(ser,temperatures):
    mesg = str(temperatures[0])+'\n'
    ser.write(mesg.encode())
    mesg = str(temperatures[1])+'\n'
    ser.write(mesg.encode())
    mesg = str(temperatures[2])+'\n'
    ser.write(mesg.encode())
    mesg = str(temperatures[3])+'\n'
    ser.write(mesg.encode())
    mesg = str(temperatures[4])+'\n'
    ser.write(mesg.encode())
    mesg = str(temperatures[5])+'\n'
    ser.write(mesg.encode())
    mesg = str(temperatures[6])+'\n'
    ser.write(mesg.encode())
    

def read_voltage(ser):
    a = ser.read(15)
    voltages = []
    voltages.append(int.from_bytes(a[0:2], "little",signed=True))
    voltages.append(int.from_bytes(a[2:4], "little",signed=True))
    voltages.append(int.from_bytes(a[4:6], "little",signed=True))
    voltages.append(int.from_bytes(a[6:8], "little",signed=True))
    voltages.append(int.from_bytes(a[8:10], "little",signed=True))
    voltages.append(int.from_bytes(a[10:12], "little",signed=True))
    voltages.append(int.from_bytes(a[12:14], "little",signed=True))
    return voltages


def main():
    global mode
    global instructions
    
    temperature_tracker = np.full(600,300.0)
    
    thread = threading.Thread(target=user_input)
    thread.daemon = True
    thread.start()
    
    
    voltmeter = volt_calculator();
    
    
    ser = serial.Serial('COM9', 115200,timeout=5)
    time.sleep(1)
    ser.reset_input_buffer()
    ser.reset_output_buffer()
    
    t0 = time.time()
    
    pre_experiment_timer = 1*60
    temp_threshold = 0.002
    target_temperature = 0.280
    
    while True:
        # if time.time() - t0 < 5:
        #     mode = Command.MAN
        #     instructions = int('0000',2)
        # elif time.time() - t0 < pre_experiment_timer or target_temperature > 0.505:
        #     mode = Command.AUTOPILOT
        #     instructions = Sequence.RUN.value
        # elif np.max(np.abs(temperature_tracker-target_temperature)) < temp_threshold:
        #     #go to next temperature setpoint
        #     target_temperature += 0.010      
        # elif temperature_tracker[0] > 1:
        #     #cryocooler needs to be cycled
        #     #restart experiments once cycling is done
        #     mode = Command.AUTOPILOT
        #     instructions = Sequence.CYCLEPUMPS.value
        #     target_temperature = 0.280           
        # else:
        #     mode = Command.AUTO
        #     instructions = Sequence.OTHER.value
        
        
        command = struct.unpack('<b', ser.read(1))[0]
        if command == Command.VOLTAGES.value:
            voltmeter.add_new_measurement(read_voltage(ser))
        elif command == Command.TEMPERATURES.value: 
            voltages = voltmeter.calculate_voltages()
            temperatures = voltmeter.calculate_temperature(voltages)
            write_command(ser, Command.ACKNOWLEDGEMENT)
            write_temperatures(ser,temperatures)
            voltmeter.reset_memory()
    #            volt_logger.info('{}, {}, {}, {}, {}, {} , {}'.format(round(voltages[0][0],2), round(voltages[1][0],2),  round(voltages[2][0],2),  round(voltages[3][0],2),  round(voltages[4][0],2),  round(voltages[5][0],2),  round(voltages[6][0],2)))
            temp_logger.info('{}, {}, {}, {}, {}, {} , {}'.format(round(temperatures[0],4), round(temperatures[1],4),  round(temperatures[2],2),  round(temperatures[3],2),  round(temperatures[4],2),  round(temperatures[5],2),  round(temperatures[6],2)))
            temperature_tracker = np.roll(temperature_tracker,1)
            temperature_tracker[0] = temperatures[0]
            # if run.is_set():
            #     q.put((time.time()-t0,*temperatures))
        elif command == Command.ACKNOWLEDGEMENT.value: 
            write_command(ser, Command.ACKNOWLEDGEMENT)
        elif command == Command.MODE.value: 
            write_command(ser, mode)
            write_i8(ser,instructions)
            if instructions == Sequence.OTHER.value:
                mesg = str(target_temperature)+'\n'
                ser.write(mesg.encode())
                
        elif command == Command.INFO.value:
            message = ser.readline()
            print("INFO: " + message.decode().rstrip('\n'))
            # comm_logger.info(message.decode().rstrip('\n'))
        else:
            print(command)

mode = Command.MAN
instructions = int('0000',2)
    
if __name__ == '__main__':
    t0 = time.time()
    main()
