from TeensyCommunication import TeensyCommunication
import numpy as np
import DualLogger
import time
import csv
from scipy.interpolate import interp1d
import threading
import Thermometers
import LivePlot

def sigfigs(x):
    a = x*10**int(1+np.log10(x))
    
    if int(str(a)[0])==1:
        ret = round(x, -int(np.floor(np.log10(np.abs(x))))+1)
    else:
        # not a 1
        ret = round(x, -int(np.floor(np.log10(np.abs(x)))))
    return ret

def mVtoR_He3(V):
    return 4.438715402514645*V - 65.45195638779182  #from calibration 07/05/19

def mVtoR_He4(V):
    return 1.3387753721881892*V - 124.47354417022443 #from calibration 07/05/19

def mVtoRerr_He3(V, sigma_V):
    return V*0.030869340521269124 + 29.883740251127055 + 4.438715402514645*sigma_V #from calibration 07/05/19

def mVtoRerr_He4(V, sigma_V):
    return V*0.004038442690370698 + 7.7866030117882294 + 1.3387753721881892*sigma_V #from calibration 07/05/19

def load_diode_calibration(filename='diode_calib.csv'):
    T0 = np.array([])
    V0 = np.array([])
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            V0 = np.append(V0,float(row[1]))
            T0 = np.append(T0,float(row[0]))
    return interp1d(V0, T0)

def load_thermistor_calibration(filename='resistor_calib.csv'):
    T0 = np.array([])
    R0 = np.array([])
    with open(filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            R0 = np.append(R0,float(row[1]))
            T0 = np.append(T0,float(row[0]))
    return interp1d(R0, T0)

def mode_input():
    while True:
        global mode
        time.sleep(1)
        mode = input()
        print("Forwarding ", mode, " onto teensy")
        with serial_lock:
            voltmeter.send_message(mode)
        print("Sent")
        DualLogger.comm_logger.info('Sent {} to Teensy'.format(mode))            


if __name__ == "__main__":
    try:
    
        # read in calibration from cryocooler manufacturers
        diode_cal = load_diode_calibration()
        thermistor_cal = load_thermistor_calibration()
    
        # define thermometers
        A = Thermometers.Thermistor(mVtoR_He3,mVtoRerr_He3,thermistor_cal)
        B = Thermometers.Thermistor(mVtoR_He4,mVtoRerr_He4,thermistor_cal)
        C = Thermometers.DiodeThermometer(diode_cal)
        D = Thermometers.DiodeThermometer(diode_cal)
        E = Thermometers.DiodeThermometer(diode_cal)
        F = Thermometers.DiodeThermometer(diode_cal)
        G = Thermometers.DiodeThermometer(diode_cal)
    
        # establish communication with the teensy
        voltmeter = TeensyCommunication('COM9')
        
        serial_lock = threading.Lock()
        thread = threading.Thread(target=mode_input)
        thread.daemon = True
        thread.start()
            
        run = threading.Event()
        run.set()
        s = LivePlot.Superplot("somePlot")
        q, q_terminate = s.start()
    
        print("Enter '0' for only temperature monitor")
        print("Enter '1' to precool from nitrogen temperatures, after which it will automatically cooldown"),
        print("Enter '2' to cooldown, after which it will go in auto-cycle mode")
        print("Enter '3' to cycle pumps from cold, after which it will automatically cooldown")
        print("Enter '4' to auto-cycle if He3 head rises above 800mK")
        print("Enter '5' to turn on He4 switch")
        print("Enter '6' to turn off He4 switch")
        print("Enter '7' to turn on He3 switch")
        print("Enter '8' to turn off He3 switch")
        print("Enter '9' to turn on He4 pump")
        print("Enter '10' to turn off He4 pump")
        print("Enter '11' to turn on He3 pump")
        print("Enter '12' to turn off He3 pump")
        #Start a timer that is used to keep track of when to log
        logging_timer = time.time()
        t0 = time.time()
        while True:
            with serial_lock:
                message = voltmeter.receive_message()
                if message == "begin_temperature":
                    voltmeter.send_message("P")
                    V_A, V_B, V_C, V_D, V_E, V_F, V_G = voltmeter.get_temperature_data()
                    DualLogger.comm_logger.info('Voltages {}, {}, {}, {}, {}, {} , {}'.format(V_A, V_B, V_C, V_D, V_E, V_F, V_G))
                    A.set_voltage(V_A)
                    B.set_voltage(V_B)
                    C.set_voltage(V_C)
                    D.set_voltage(V_D)
                    E.set_voltage(V_E)
                    F.set_voltage(V_F)
                    G.set_voltage(V_G)
                    voltmeter.send_message(str(A.T)+'\n')
                    voltmeter.send_message(str(B.T)+'\n')
                    voltmeter.send_message(str(C.T)+'\n')
                    voltmeter.send_message(str(D.T)+'\n')
                    voltmeter.send_message(str(E.T)+'\n')
                    voltmeter.send_message(str(F.T)+'\n')
                    voltmeter.send_message(str(G.T)+'\n')
    
                    if run.is_set():
                        q.put((time.time()-t0,A.T, B.T, C.T, D.T, E.T, F.T, G.T))
                        
                    #log every measurement    
#                    DualLogger.temp_logger.info('{}, {}, {}, {}, {}, {} , {}'.format(A.T, B.T, C.T, D.T, E.T, F.T, G.T))
                    # Log the temperature every 60 seconds
                    if time.time() - logging_timer > 60:
                        DualLogger.temp_logger.info('{}, {}, {}, {}, {}, {} , {}'.format(A.T, B.T, C.T, D.T, E.T, F.T, G.T))
                        logging_timer = time.time()
                elif message == "PING":
                    voltmeter.send_message("P")
                else:
                    print(message)
            time.sleep(0.1)
    except KeyboardInterrupt:
        q_terminate.put(1)
        run.clear()
#    except BaseException  as err:
#        print(err)
#        DualLogger.comm_logger.exception(err)