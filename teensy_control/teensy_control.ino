// set this to the hardware serial port you wish to use
#include <PID_v1.h>
#define POWERSUPPLY Serial1
#define DISPLAY Serial2

// define teensy pins
const int ledPin = 13;
const int voltAPin = 16;
const int voltBPin = 15;
const int voltCPin = 21;
const int voltDPin = 20;
const int voltEPin = 19;
const int voltFPin = 18;
const int voltGPin = 17;
const int ACDrivePin = 23;
const int He3switchPin = 6;
const int He4switchPin = 5;

// set up parameters used to keep track of lock in 
int ACState = LOW;
int multiplier = 1;
boolean just_flipped = false;
unsigned long currentMillis = 0;
unsigned long previousMillis = 0; // will store last time the current switched direction
const float measures_per_cycle = 40; //The number of measurements per cycle, where each takes ~1.25 ms

//Define variables we'll be connecting to our PIDs
double He3pump_input, He3pump_output, He3pump_setpoint;
double He4pump_input, He4pump_output, He4pump_setpoint;

//Specify the links and initial tuning parameters for PIDs
//Reversed as increase current = increase temperature = decrease resistance
PID He3pump_PID(&He3pump_input, &He3pump_output, &He3pump_setpoint,0.010,0.0003,0, REVERSE); //lets start with Kp=2, Ki=5, Kp=0
PID He4pump_PID(&He4pump_input, &He4pump_output, &He4pump_setpoint,0.010,0.0003,0, REVERSE); //lets start with Kp=2, Ki=5, Kp=0

float He3pump_voltage, He4pump_voltage, He3switch_voltage, He4switch_voltage;

// The teensy can operatare in  four modes
// mode = 0 is 'monitor' mode. The other modes are of finite duration, and it will always return to this mode
// mode = 1 is 'precool' mode. This can be run when the cryocooler is cooling to 4K after a He transfer. It warms the charcoal pumps to prepare for a cooldown
// mode = 2 is 'cooldown' mode. This turns the pump heaters off and turns the switches on, which initiates cooling to around 250mK
// mode = 3 is 'stay cold' mode. This is a manual override that is useful if I'm already at 250mK but had to poewr cycle something
char mode = '0'; 

void setup() {
  // put teensy ADC into high resolution mode
  analogReadRes(13); 
  // open serial port to python
  Serial.begin(9600);
  //open serial port to power supply
  POWERSUPPLY.begin(19200,SERIAL_8N1);
  // prepare all used teensy pins
  pinMode(ledPin, OUTPUT);
  pinMode(voltAPin, INPUT);
  pinMode(voltBPin, INPUT);
  pinMode(voltCPin, INPUT);
  pinMode(voltDPin, INPUT);
  pinMode(voltEPin, INPUT);
  pinMode(voltFPin, INPUT);
  pinMode(voltGPin, INPUT);
  pinMode(ACDrivePin, OUTPUT);  
  pinMode(He3switchPin, OUTPUT);  
  digitalWrite(He3switchPin, LOW);
  pinMode(He4switchPin, OUTPUT);
  digitalWrite(He4switchPin, LOW);

  //  Start with all power supply channels off
  POWERSUPPLY.print("OPALL 0\n"); 
  POWERSUPPLY.print("V1 0\n");
  POWERSUPPLY.print("I1 0\n");
  POWERSUPPLY.print("V2 0\n");
  POWERSUPPLY.print("I2 0\n");

  He3pump_PID.SetMode(AUTOMATIC);
  He4pump_PID.SetMode(AUTOMATIC);
}

unsigned long precool_timer = 0;
boolean is_warm = false;
boolean precool_complete = false;

boolean timer_check(){
  float voltage_40K = 1084.8; //voltage corresponding to 40K
  float voltage_45K = 1077.3; //voltage corresponding to 45K
  float voltage_50K = 1068.7; //voltage corresponding to 50K

  if ((He3pump_voltage < voltage_45K) && (He4pump_voltage < voltage_50K)){
    //if this the the first time above these temperatures, set the precool timer running
    if (is_warm == false){
      precool_timer = millis();
      Serial.print("Precool timer started, counting 40min from now");
    }

    if ((millis() - precool_timer > 1000*60*40) && (is_warm == true)){
      precool_complete = true;
    }
    is_warm = true;
  }

  if ((He3pump_voltage > voltage_40K) && (He4pump_voltage > voltage_45K)){
    // if temperatures dip by more than 5K, then turn off is_warm status
    is_warm = false;
  }
  return is_warm;
}

void check_switches_off(){
  //   If either switch warmer than 12K, don't do precool yet as switches need to be OFF
  if ((He3switch_voltage < 1381.3) || (He4switch_voltage < 1381.3)){
    Serial.println("Abandoning precool, a switch is still on");
    mode = '0';
  }
}

void He3_rapid_heat(){
    POWERSUPPLY.write("V1 16\n"); 
    POWERSUPPLY.write("I1 0.060\n"); //limit current to the upper current the manual suggests
    POWERSUPPLY.write("OP1 1\n");
}

void He3_PID(float He3pump_input,float He3pump_setpoint){
    He3pump_PID.Compute();
    String He3current_command = "I1 " + String(He3pump_output,3).substring(0,5) + "\n"; //Take first four character of the output. This should be "0.xxx"
    POWERSUPPLY.print("V1 16\n"); //set limit voltage supply to 24V,  incase the PID says outputs something stupid
    POWERSUPPLY.print(He3current_command); //this used to be POWERSUPPLY.write, but I think this works to
    POWERSUPPLY.write("OP1 1\n");
}

void He4_rapid_heat(){
    POWERSUPPLY.write("V2 28\n");
    POWERSUPPLY.write("I2 0.130\n"); //limit current to the upper current the manual suggests
    POWERSUPPLY.write("OP2 1\n");
}

void He4_PID(float He4pump_input, float He4pump_setpoint){
    He4pump_PID.Compute();
    String He4current_command = "I2 " + String(He4pump_output,3).substring(0,5) + "\n"; //Take first four character of the output. This should be "0.xxx"
    POWERSUPPLY.print("V2 28\n"); //set limit voltage supply to 25V, incase the PID says outputs something stupid
    POWERSUPPLY.print(He4current_command); //this used to be POWERSUPPLY.write, but I think this works to
    POWERSUPPLY.write("OP2 1\n");
}

void He3_pump(boolean status){
  float voltage_35K = 1092.0;
  float voltage_48K = 1072.8;
  if (status == true){
    Serial.print("He3 pump turned on");
    if (He3pump_voltage > voltage_35K){
      He3_rapid_heat();
    } else {
      He3_PID(He3pump_voltage, voltage_48K);
    }
  } else {
    //  turn off He3 pump
    Serial.print("He3 pump turned off");
    POWERSUPPLY.write("V1 0\n");
    POWERSUPPLY.write("I1 0\n");
    POWERSUPPLY.write("OP1 0\n");
  }
}

void He4_pump(boolean status){
  float voltage_40K = 1084.8;
  float voltage_55K = 1061.7;
  if (status == true){
    Serial.print("He4 pump turned on");
    if (He4pump_voltage > voltage_40K){
      He4_rapid_heat();
    } else {
      He3_PID(He4pump_voltage, voltage_55K);    
    }
  } else {
    //  turn off He4 pump
    Serial.print("He4 pump turned off");
    POWERSUPPLY.write("V2 0\n");
    POWERSUPPLY.write("I2 0\n");
    POWERSUPPLY.write("OP2 0\n");
  }
}

void He4_switch(boolean status){
  if (status == true){
    POWERSUPPLY.write("OP3 1\n");
    digitalWrite(He4switchPin, HIGH);
    Serial.print("He4 switch turned on");
  } else {
    digitalWrite(He4switchPin, LOW);
    Serial.print("He4 switch turned off");
  }
}

void He3_switch(boolean status){
  if (status == true){
    POWERSUPPLY.write("OP3 1\n");
    digitalWrite(He3switchPin, HIGH);
    Serial.print("He3 switch turned on");
  } else {
    digitalWrite(He3switchPin, LOW);
    Serial.print("He3 switch turned off");
  }
}


void precool(){
  //  Need to raise He3 pump to 45-50K, and He4 pump to 50-60K, and keep them there while heads cool to 4-5K.
  //  Stay there for 40 minutes before the final cooldown procedure begins
  while (true){
    //  read diodes
    He3pump_voltage = analogRead(voltEPin)*3300.0/8191.0;
    He4pump_voltage = analogRead(voltDPin)*3300.0/8191.0;    
    He3switch_voltage = analogRead(voltGPin)*3300.0/8191.0;
    He4switch_voltage = analogRead(voltFPin)*3300.0/8191.0;

    check_switches_off();

    if (timer_check() == true){
      // pumps have been warm for 40 min
      break;
    }

    He3_pump(true);
    He4_pump(true);

    measure_temperature(); //this takes a bit over 1 second
    delay(1000*5);
  }
  Serial.print("Precool complete, entering cooldown mode");
  mode = '2';  
}

void cooldown(){
  unsigned long cooldown_start = millis();
  unsigned long minutes = 1000*60;
  unsigned long t;
  while (true){
    t = millis() - cooldown_start;
    if (t < 5 * minutes){
      // don't do anything yet
      He4_pump(true);
      He4_switch(false);
      He3_pump(true);
      He3_switch(false);
    } else if (t < 10 * minutes){
      //turn off He4 pump
      He4_pump(false);
      He4_switch(false);
      He3_pump(true);
      He3_switch(false);
    } else if (t < 20 * minutes){
      //turn on He4 switch
      He4_pump(false);
      He4_switch(true);
      He3_pump(true);
      He3_switch(false);
    } else if (t < 30 * minutes){
      //turn off He3 pump
      He4_pump(false);
      He4_switch(true);
      He3_pump(false);
      He3_switch(false);
    } else if (t < 45 * minutes){
      //turn on He3 switch
      He4_pump(false);
      He4_switch(true);
      He3_pump(false);
      He3_switch(true);
    } else if (t >= 45*minutes){
      break;
    }
    measure_temperature();
    delay(1000*5);
  }
  Serial.print("Cooldown complete, entering monitor mode");
  mode = '0';
}

void flip_polarity(){
  // if the current is normal then reversed it and vice-versa:
  if(ACState == LOW){
    ACState = HIGH;
    multiplier = -1;
    just_flipped = false;
  } else { 
    ACState = LOW;
    multiplier = 1;
    //record that cycle just started again
    just_flipped = true;
  }
  // implement new current direction
  digitalWrite(ACDrivePin, ACState);
  delay(1);
}

void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
}  

void measure_temperature() {
  //tell python that I'm about to read the temperature
  delay(1);
  Serial.println("begin_temperature");
  delay(1);

//    wait for confirmation from python
  int waiting_counter = 0;
   while (Serial.available()==0){
     delay(10);
     waiting_counter++;
     if (waiting_counter>100){
      //skip temperature measurement if python isn't here
      return;
     }
   }
   serialFlush();
  // reset to always the same current polarity
  ACState = LOW;
  digitalWrite(ACDrivePin, ACState);
  delay(1);

  unsigned long startMillis = millis();
 
  int print_counter = 0;
  
  while (true){
    //if half the cycle time has passed, flip current polarity
    if (print_counter >= measures_per_cycle/2){
      flip_polarity();
      print_counter = 0;
    } else{
      just_flipped = false;
    }
    
    //  Stop when have been measuring for more than 1 second and current just went back to LOW
    if ((millis() - startMillis > 1000) && (just_flipped == true)){
      break;
    }
    
    //read thermometers
    String voltA_value = String(multiplier*analogRead(voltAPin));
    String voltB_value = String(multiplier*analogRead(voltBPin));
    String voltC_value = String(analogRead(voltCPin));
    String voltD_value = String(analogRead(voltDPin));
    String voltE_value = String(analogRead(voltEPin));
    String voltF_value = String(analogRead(voltFPin));
    String voltG_value = String(analogRead(voltGPin));
      
    Serial.println(voltA_value + " " + voltB_value + " " + voltC_value + " " + voltD_value + " " + voltE_value + " " + voltF_value + " " + voltG_value);      
    print_counter++;
    delay(1);    
  }
  delay(1);
  Serial.println("end_temperature");
  delay(1); 
}

boolean just_turned_on = true;


void loop() {
  //wait for python to send something before getting started
  // if (just_turned_on){
  //   while (Serial.available()==0){
  //     delay(100);
  //   }
  //   just_turned_on = false;
  //   serialFlush();
  // }

  if (Serial.available()){
    mode = Serial.read();
  }

  switch (mode){
    case '0': 
      //monitor mode
      measure_temperature();
      delay(1000*5);
      break;    
    case '1':
      // precool mode
      precool();
      break;
    case '2':
      // cooldown mode
      cooldown();
      break;    
    case '3':
       //turn on switches
      He3_switch(true);
      He4_switch(true);
      mode = '0';
      break; 
  }
  delay(1);  
}
