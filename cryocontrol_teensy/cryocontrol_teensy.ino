#include <PID_v1.h>

//Define variables we'll be connecting to our PIDs
double He3pump_input, He3pump_output, He3pump_setpoint;
double He4pump_input, He4pump_output, He4pump_setpoint;

//Specify the links and initial tuning parameters for PIDs
//Reversed as increase current = increase temperature = decrease resistance
PID He3pump_PID(&He3pump_input, &He3pump_output, &He3pump_setpoint, 0.004, 0.0003, 0.0, DIRECT); 
PID He4pump_PID(&He4pump_input, &He4pump_output, &He4pump_setpoint, 0.010, 0.0005, 0.0, DIRECT); 

// set this to the hardware serial port you wish to use
#define POWERSUPPLY Serial1
#define SEGMENT Serial2

// define teensy pins
const int ledPin = 13;
const int voltAPin = 16;
const int voltBPin = 15;
const int voltCPin = 21;
const int voltDPin = 20;
const int voltEPin = 19;
const int voltFPin = 18;
const int voltGPin = 17;
const int ACDrivePin = 23;
const int He3switchPin = 6;
const int He4switchPin = 5;

// set up parameters used to keep track of lock in
int ACState = LOW;
int multiplier = 1;
boolean just_flipped = false;
unsigned long currentMillis = 0;
unsigned long previousMillis = 0; // will store last time the current switched direction
const float measures_per_cycle = 40; //The number of measurements per cycle, where each takes ~1.25 ms

// The teensy can operatare in  four modes
// mode = 0 is 'monitor' mode. The other modes are of finite duration, and it will always return to this mode
// mode = 1 is 'precool' mode. This can be run when the cryocooler is cooling to 4K after a He transfer. It warms the charcoal pumps to prepare for a cooldown
// mode = 2 is 'cooldown' mode. This turns the pump heaters off and turns the switches on, which initiates cooling to around 250mK
// mode = 3 is 'stay cold' mode. This is a manual override that is useful if I'm already at 250mK but had to poewr cycle something
char mode = '0';

void setup() {
  // put teensy ADC into high resolution mode
  analogReadRes(13);
  // open serial port to python
  Serial.begin(9600);
  //open serial port to power supply
  POWERSUPPLY.begin(19200, SERIAL_8N1);
  SEGMENT.begin(9600, SERIAL_8N1);

  // prepare all used teensy pins
  pinMode(ledPin, OUTPUT);
  pinMode(voltAPin, INPUT);
  pinMode(voltBPin, INPUT);
  pinMode(voltCPin, INPUT);
  pinMode(voltDPin, INPUT);
  pinMode(voltEPin, INPUT);
  pinMode(voltFPin, INPUT);
  pinMode(voltGPin, INPUT);
  pinMode(ACDrivePin, OUTPUT);
  pinMode(He3switchPin, OUTPUT);
  digitalWrite(He3switchPin, LOW);
  pinMode(He4switchPin, OUTPUT);
  digitalWrite(He4switchPin, LOW);

  //  Start with all power supply channels off
  POWERSUPPLY.print("OPALL 0\n");
  POWERSUPPLY.print("V1 0\n");
  POWERSUPPLY.print("I1 0\n");
  POWERSUPPLY.print("V2 0\n");
  POWERSUPPLY.print("I2 0\n");

  He3pump_PID.SetMode(AUTOMATIC);
  He4pump_PID.SetMode(AUTOMATIC);
}

float He3Head_temperature, He4Head_temperature, FilmBurner_temperature, He4Pump_temperature, He3Pump_temperature, He4Switch_temperature, He3Switch_temperature;


void loop() {
  if (Serial.available()) {
    mode = Serial.read();
    Serial.print("Arduino confirms it has entered mode:");
    Serial.print(mode);
    Serial.print("\n");
  }
  
  switch (mode) {
    case '0':
      break;
    case '1':
      precool();
      break;
    case '2':
      cooldown();
      break;
    case '3':
      cycle_pumps();
      break;
    case '4':
      //automatically cycle when starting to warm
      if(He3Head_temperature > 0.8){
        mode = '3';
      }
      break;
    case '5':
      //turn on He4 switch
      He4_switch(true);
      break;
    case '6':
      //turn off He4 switch
      He4_switch(false);
      break;
    case '7':
      //turn on He3 switch
      He3_switch(true);
      break;
    case '8':
      //turn off He3 switch
      He3_switch(false);
      break;
    case '9':
      //turn on He4 pump
      He4_pump(true);
      break;
    case '10':
      //turn off He4 pump
      He4_pump(false);
      break;
    case '11':
      //turn on He3 pump
      He3_pump(true);
      break;
    case '12':
      //turn off He3 pump
      He3_pump(false);
      break;
  }
  measure_temperature();
  delay(10);
}
