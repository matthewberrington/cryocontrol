void He4_switch(boolean status) {
  if (status == true) {
    POWERSUPPLY.write("OP3 1\n");
    digitalWrite(He4switchPin, HIGH);
//    Serial.print("He4 switch turned on\n");
  } else {
    digitalWrite(He4switchPin, LOW);
//    Serial.print("He4 switch turned off\n");
  }
}

void He3_switch(boolean status) {
  if (status == true) {
    POWERSUPPLY.write("OP3 1\n");
    digitalWrite(He3switchPin, HIGH);
//    Serial.print("He3 switch turned on\n");
  } else {
    digitalWrite(He3switchPin, LOW);
//    Serial.print("He3 switch turned off\n");
  }
}
  
void He3_rapid_heat() {
  POWERSUPPLY.write("V1 16\n");
  POWERSUPPLY.write("I1 0.060\n"); //limit current to the upper current the manual suggests
}

void He4_rapid_heat() {
  POWERSUPPLY.write("V2 28\n");
  POWERSUPPLY.write("I2 0.130\n"); //limit current to the upper current the manual suggests
}

void He3_pump(boolean status) { 
  if (status == true) {
    POWERSUPPLY.write("OP1 1\n");
    if (He3Pump_temperature < 35.0) {
      He3_rapid_heat();
    } else {
      He3pump_setpoint = 48.0;
      He3pump_input = He3Pump_temperature;
      He3_PID();
    }
  } else {
    //  turn off He3 pump
//    Serial.print("He3 pump turned off\n");
    POWERSUPPLY.write("V1 0\n");
    POWERSUPPLY.write("I1 0\n");
    POWERSUPPLY.write("OP1 0\n");
  }
}

void He4_pump(boolean status) {
  if (status == true) {
    POWERSUPPLY.write("OP2 1\n");
    if (He4Pump_temperature < 40.0) {
      He4_rapid_heat();
    } else {
      He4pump_setpoint = 55.0;
      He4pump_input = He4Pump_temperature;
      He4_PID();
    }
  } else {
    //  turn off He4 pump
//    Serial.print("He4 pump turned off\n");
    POWERSUPPLY.write("V2 0\n");
    POWERSUPPLY.write("I2 0\n");
    POWERSUPPLY.write("OP2 0\n");
  }
}

boolean He3_switch_is_on() {
  if (He3Switch_temperature > 12.0) {
    return true;
  } else {
    return false;
  }
}

boolean He4_switch_is_on() {
  if (He4Switch_temperature > 12.0) {
    return true;
  } else {
    return false;
  }
}

boolean He3_pump_is_on() {
  // for some reason the power supply spits back all past instructions when you first use a ?
  // so I query voltage once and throw the response away
  POWERSUPPLY.write("V1?\n");
  delay(1);
  POWERSUPPLY.readString();
  delay(1);
  //now for the actual request
  POWERSUPPLY.write("V1?\n");
  delay(1);
  String resp = POWERSUPPLY.readString();
  float resp_float = resp.remove(0,7).toFloat(); //chop off first seven characters to get just the number
boolean is_on;
  if (resp_float == 0.00){
    is_on = false;
  } else {
    is_on = true;
  }
  return is_on;  
}


boolean He4_pump_is_on() {
  // for some reason the power supply spits back all past instructions when you first use a ?
  // so I query voltage once and throw the response away
  POWERSUPPLY.write("V2?\n");
  delay(1);
  POWERSUPPLY.readString(); //throw away
  delay(1);
  //now for the actual request
  POWERSUPPLY.write("V2?\n");
  delay(1);
  String resp = POWERSUPPLY.readString();
  float resp_float = resp.remove(0,7).toFloat(); //chop off first seven characters to get just the number
  boolean is_on;
  if (resp_float == 0.00){
    is_on = false;
  } else {
    is_on = true;
  }
  return is_on;  
}

void flip_polarity() {
  // if the current is normal then reversed it and vice-versa:
  if (ACState == LOW) {
    ACState = HIGH;
    multiplier = -1;
    just_flipped = false;
  } else {
    ACState = LOW;
    multiplier = 1;
    //record that cycle just started again
    just_flipped = true;
  }
  // implement new current direction
  digitalWrite(ACDrivePin, ACState);
  delay(1);
}

void measure_temperature() {
  Serial.println("begin_temperature");
  previousMillis = millis();
  int timeout = 1000;
  char response;
  while(true){
    if(Serial.available()){
      response = Serial.read();
      break; //exit loop and let code keep running
    }
    if(millis()-previousMillis > timeout){
      return; //Cancel the measurement if the PC response timesout
    }
  }
  
  if(response != 'P'){
    mode = response;
    return; //Cancel the measurement if the PC isn't responding to begin_temperature request
  }
 
  

  // reset to always the same current polarity
  ACState = LOW;
  digitalWrite(ACDrivePin, ACState);
  delay(1);

  unsigned long startMillis = millis();

  int counter = 0;

  while (true) {
    //  Stop when have been measuring for more than 1 second and current just went back to LOW
    if ((millis() - startMillis > 1000) && (just_flipped == true)) {
      break;
    }

    //read thermometers
    String voltA_value = String(multiplier * analogRead(voltAPin));
    String voltB_value = String(multiplier * analogRead(voltBPin));
    String voltC_value = String(analogRead(voltCPin));
    String voltD_value = String(analogRead(voltDPin));
    String voltE_value = String(analogRead(voltEPin));
    String voltF_value = String(analogRead(voltFPin));
    String voltG_value = String(analogRead(voltGPin));

    Serial.println(voltA_value + " " + voltB_value + " " + voltC_value + " " + voltD_value + " " + voltE_value + " " + voltF_value + " " + voltG_value);
    counter++;
    
    //if half the cycle time has passed, flip current polarity
    if (counter >= measures_per_cycle/2) {
      flip_polarity();
      counter = 0;
    } else {
      just_flipped = false;
    }
    delay(1);
  }
  delay(1);
  Serial.println("end_temperature");
  Serial.flush();
  delay(10);
  He3Head_temperature = Serial.readStringUntil('\n').toFloat();
  He4Head_temperature = Serial.readStringUntil('\n').toFloat();
  FilmBurner_temperature = Serial.readStringUntil('\n').toFloat();
  He4Pump_temperature = Serial.readStringUntil('\n').toFloat();
  He3Pump_temperature = Serial.readStringUntil('\n').toFloat();
  He4Switch_temperature = Serial.readStringUntil('\n').toFloat();
  He3Switch_temperature = Serial.readStringUntil('\n').toFloat();
  numeric_message(He3Head_temperature);
  
  delay(1);
}
