
void He3_PID() {
  He3pump_PID.Compute();
  String He3current_command = "I1 " + String(He3pump_output, 3).substring(0, 5) + "\n"; //Take first four character of the output. This should be "0.xxx"
  POWERSUPPLY.print("V1 16\n"); //set limit voltage supply to 24V,  incase the PID says outputs something stupid
  POWERSUPPLY.print(He3current_command); //this used to be POWERSUPPLY.write, but I think this works to
  POWERSUPPLY.write("OP1 1\n");
}

void He4_PID() {
  He4pump_PID.Compute();
  String He4current_command = "I2 " + String(He4pump_output, 3).substring(0, 5) + "\n"; //Take first four character of the output. This should be "0.xxx"
//  Serial.print("He4pump_output: ");
//  Serial.println(He4pump_output);
  POWERSUPPLY.print("V2 28\n"); //set limit voltage supply to 25V, incase the PID says outputs something stupid
  POWERSUPPLY.print(He4current_command); //this used to be POWERSUPPLY.write, but I think this works to
  POWERSUPPLY.write("OP2 1\n");
}
