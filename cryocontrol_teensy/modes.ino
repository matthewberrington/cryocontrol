int counter = 0;
boolean beginning_precool = false;

//boolean timer_passed_40min() {
//  if ((He3Pump_temperature > 45.0) && (He4Pump_temperature > 50.0)) {
//    //if this the the first time above these temperatures, set the precool timer running
//    if (is_warm == false) {
//      is_warm = true;
//      precool_timer = millis();
//      Serial.print("Precool timer started, counting 40min from now\n");
//    }
//
//    if ((millis() - precool_timer > 1000 * 60 * 40) && (is_warm == true)) {
//      return true;
//    }
//  }
//
//  if ((He3Pump_temperature < 40.0) && (He4Pump_temperature < 45.0)) {
//    // if temperatures dip by more than 5K, then turn off is_warm status
//    is_warm = false;
//  }
//  return false;
//}

boolean printed_msg = false;

void precool() {
  //  Need to raise He3 pump to 45-50K, and He4 pump to 50-60K, and keep them there while heads cool to 4 or 5K.
  //  Keep pumps warm for 20 min after heads cool beneath 5K
  
  if (He3_switch_is_on() || He4_switch_is_on()){
    if(!printed_msg) {
      Serial.println("A switch is still on, waiting for them to turn off\n");
      printed_msg = true;
    }
    He3_switch(false);
    He4_switch(false);
    He3_pump(false);
    He4_pump(false);
  } else {
    printed_msg = false;
    if (!He4_pump_is_on()){
      Serial.println("Turning on He4 pump");
    }
    if (!He3_pump_is_on()){
      Serial.println("Turning on He3 pump");
    } 
    He3_pump(true);
    He4_pump(true);
  }

  // If both heads are below 5K, then increment a counter
  if (He4Head_temperature < 5.0 && He3Head_temperature < 5.0){
    counter++;
  } else {
    counter = 0;
  }

  // Once I've measured 1200 cycles below 5K, roughly 20min, then precool is complete
  if (counter > 1200){
    counter = 0;
    Serial.print("Precool complete, entering cooldown mode\n");
    mode = '2';
    delay(100);
  }      
  delay(10);
}

void cycle_pumps() {
  //  Need to raise He3 pump to 45-50K, and He4 pump to 50-60K
  //  Stay there for 30 minutes before cooling down again
  
  if (He3_switch_is_on() || He4_switch_is_on()){
    if(!printed_msg) {
      Serial.println("A switch is still on, waiting for them to turn off\n");
      printed_msg = true;
    }
    He3_switch(false);
    He4_switch(false);
    He3_pump(false);
    He4_pump(false);
  } else {
    printed_msg = false;
    if (!He4_pump_is_on()){
      Serial.println("Turning on He4 pump");
    }
    if (!He3_pump_is_on()){
      Serial.println("Turning on He3 pump");
    } 
    He3_pump(true);
    He4_pump(true);
  }

  // If both pumps are warm, increment a counter
  if (He4Pump_temperature > 50.0 && He3Pump_temperature > 45.0){
    counter++;
  } else {
    counter = 0;
  }

  // Once I've measured 1800 cycles with pumps warm, roughly 30 min, then cycling is complete
  if (counter > 1800){
    counter = 0;
    Serial.print("Pump cycle complete, entering cooldown mode\n");
    mode = '2';
    delay(100);
  }      
  delay(10);
}

unsigned long step_start;
unsigned long minutes = 1000 * 60;
unsigned long t;

int cooldown_step = 0;

void cooldown() {
  if (cooldown_step == 0){
    cooldown_step = 1;
    step_start = millis();
  }
  
  t = millis() - step_start;
  
  if (cooldown_step == 1) {
    //turn off He4 pump
    He4_pump(false);
    He4_switch(false);
    He3_pump(true);
    He3_switch(false);

    if (t > 5 * minutes){
      cooldown_step = 2;
      step_start = millis();
    }
  } else if (cooldown_step == 2) {
    //turn on He4 switch
    He4_pump(false);
    He4_switch(true);
    He3_pump(true);
    He3_switch(false);
    
    if (He4Head_temperature < 2.0 && He3Head_temperature < 2.0){
      cooldown_step = 3;
      step_start = millis();
    }
  } else if (cooldown_step == 3) {
    //turn off He3 pump
    He4_pump(false);
    He4_switch(true);
    He3_pump(false);
    He3_switch(false);

    if (t > 5 * minutes){
      cooldown_step = 4;
      step_start = millis();
    }
  } else if (cooldown_step == 4) {
    //turn on He3 switch
    He4_pump(false);
    He4_switch(true);
    He3_pump(false);
    He3_switch(true);

    if (t > 30 * minutes){
      cooldown_step = 5;
      step_start = millis();
    }
  } else if (cooldown_step == 5) {
  Serial.print("Cooldown complete, entering auto-cycle mode");
  cooldown_step = 0;
  mode = '4';
  }
}
