# -*- coding: utf-8 -*-
"""
This example is identical to https://gist.github.com/Overdrivr/efea3d363556c0dcf4b6
Except that here the plot is contained in a class.
The superplot.start method starts the graph and returns a standard multiprocessing.queue
the io function puts data in this queue, while the graph empties it regularly
The outcome is :
- a super fast application thanks to PyQtGraph
- a main process that is never blocked by the graph
Enjoy !
"""
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
from multiprocessing import Process, Manager, Queue
import sched, time, threading

class QMainWindowCustom(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        # when you want to destroy the dialog set this to True
        self._want_to_close = False

    def closeEvent(self, evnt):
        if self._want_to_close:
            super(QMainWindowCustom, self).closeEvent(evnt)
        else:
            evnt.ignore()
            self.setWindowState(QtCore.Qt.WindowMinimized)
    
   
# This function is responsible for displaying the data
# it is run in its own process to liberate main process
class Superplot():
    def __init__(self,name):
        self.name = name
        # Process-local buffers used to host the displayed data
        self.fast_history = 5*60 #Have fast plot show most recent 5 minutes
        self.slow_history = 24*60*60 #Have fast plot show most recent 24 hours
        
        self.fast_data_points = 100        
        self.T_fast = np.empty((0,7))
        self.time_fast = np.empty(0)
        self.T_slow = np.empty((0,7))
        self.time_slow = np.empty(0)
        self.counter = 0
        self.let_close = False
        
        #ratio of how many fast data points to every slow data point
        self.fast_to_slow = 60
        self.counter2 = 0
        


    def start(self):
        self.q = Queue()
        self.q_terminate = Queue()
        self.p = Process(target=self.run)
        self.p.start()
        return (self.q, self.q_terminate)

    def join(self):
        self.p.join()

    def _update(self):
        if not self.q_terminate.empty():
            self.mw._want_to_close = True
#            self.mw.closeEvent()
            
        while not self.q.empty():
            #get temperature data out of the queue
            data = self.q.get()
            t = data[0]
            T = data[1:]
            
            #If the T_fast array is as long as the history I want to record, then
            #roll the array and replace the last data point
            #else append to the array
            if len(self.time_fast) > 0 and t - self.time_fast[0] > self.fast_history:
                self.T_fast = np.roll(self.T_fast,-7)
                self.T_fast[-1] = T
                self.time_fast = np.roll(self.time_fast,-1)
                self.time_fast[-1] = t
            else:
                self.T_fast = np.vstack((self.T_fast,T))
                self.time_fast = np.append(self.time_fast,t)
            
            #If the T_skiw array is as long as the history I want to record, then
            #roll the array and replace the last data point
            #else append to the array
            if self.counter % self.fast_to_slow == 0:
                if len(self.time_slow) > 0 and t - self.time_slow[0] > self.slow_history:
                    self.T_slow = np.roll(self.T_slow,-7)
                    recent_mean_T = np.mean(self.T_fast[-len(self.fast_to_slow):],axis=0)
                    self.T_slow[-1] = recent_mean_T #Add the average of the new fast data points
                    self.time_slow = np.roll(self.time_slow,-1)
                    self.time_slow[-1] = t
                else:
                    recent_mean_T = np.mean(self.T_fast[(-len(self.T_fast)//self.fast_to_slow):],axis=0)
                    self.T_slow = np.vstack((self.T_slow,recent_mean_T)) #Add the average of the new fast data points
                    self.time_slow = np.append(self.time_slow,t)
            self.counter+=1

            slow_time_axis = (self.time_slow-np.max(self.time_slow))/(60*60)
            self.curve1A.setData(slow_time_axis,np.take(self.T_slow,0,axis=1))
            self.curve1B.setData(slow_time_axis,np.take(self.T_slow,1,axis=1))
            self.curve1C.setData(slow_time_axis,np.take(self.T_slow,2,axis=1))
            self.curve1D.setData(slow_time_axis,np.take(self.T_slow,3,axis=1))
            self.curve1E.setData(slow_time_axis,np.take(self.T_slow,4,axis=1))
            self.curve1F.setData(slow_time_axis,np.take(self.T_slow,5,axis=1))
            self.curve1G.setData(slow_time_axis,np.take(self.T_slow,6,axis=1))

            fast_time_axis = (self.time_fast - np.max(self.time_fast))/60
            self.curve2A.setData(fast_time_axis,np.take(self.T_fast,0,axis=1))
            self.curve2B.setData(fast_time_axis,np.take(self.T_fast,1,axis=1))
            self.curve2C.setData(fast_time_axis,np.take(self.T_fast,2,axis=1))
            self.curve2D.setData(fast_time_axis,np.take(self.T_fast,3,axis=1))
            self.curve2E.setData(fast_time_axis,np.take(self.T_fast,4,axis=1))
            self.curve2F.setData(fast_time_axis,np.take(self.T_fast,5,axis=1))
            self.curve2G.setData(fast_time_axis,np.take(self.T_fast,6,axis=1))


    def run(self):

        app = QtGui.QApplication([])
        
        self.mw = QMainWindowCustom()
        self.mw.setWindowTitle('pyqtgraph example: PlotWidget')
        self.mw.resize(800,500)
        cw = QtGui.QWidget()
        self.mw.setCentralWidget(cw)
        l = QtGui.QHBoxLayout()
        cw.setLayout(l)
        
        p1 = pg.PlotWidget(name='Plot1')  ## giving the plots names allows us to link their axes together
        p1.plot()
        l.addWidget(p1)
        p2 = pg.PlotWidget(name='Plot2')
        p2.plot()
        l.addWidget(p2)
        self.mw.show()
        
        
        
        p1.setDownsampling(mode='peak')
        p2.setDownsampling(mode='peak')
        p1.setClipToView(True)
        p2.setClipToView(True)
        p1.setLogMode(y = True)
        p2.setLogMode(y = True)
        p1.addLegend()
        p2.addLegend()

        p1.showGrid(x = True, y = True, alpha = 0.4)
        p2.showGrid(x = True, y = True, alpha = 0.4)

        p1.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
        p1.setLabel('bottom', "Time", units='hours', **{'color': '#FFF', 'font-size': '12pt'})
        p2.setLabel('left', "Temperature", units='K', **{'color': '#FFF', 'font-size': '12pt'})
        p2.setLabel('bottom', "Time", units='minutes', **{'color': '#FFF', 'font-size': '12pt'})
                                                          
        self.curve1A = p1.plot(pen=(250,129,116), name="3He Head")
        self.curve1B = p1.plot(pen=(129,177,210), name="4He Head")
        self.curve1C = p1.plot(pen=(253,180,98), name="Film Burner")
        self.curve1D = p1.plot(pen=(179,222,105), name="4He Pump")
        self.curve1E = p1.plot(pen=(188,130,189), name="3He Pump")
        self.curve1F = p1.plot(pen=(204,235,196), name="4He Switch")
        self.curve1G = p1.plot(pen=(255,237,111), name="3He Switch")
        
        self.curve2A = p2.plot(pen=(250,129,116), name="3He Head")
        self.curve2B = p2.plot(pen=(129,177,210), name="4He Head")
        self.curve2C = p2.plot(pen=(253,180,98), name="Film Burner")
        self.curve2D = p2.plot(pen=(179,222,105), name="4He Pump")
        self.curve2E = p2.plot(pen=(188,130,189), name="3He Pump")
        self.curve2F = p2.plot(pen=(204,235,196), name="4He Switch")
        self.curve2G = p2.plot(pen=(255,237,111), name="3He Switch")
        
        ## Start a timer to rapidly update the plot in pw
        t = QtCore.QTimer()
        t.timeout.connect(self._update)
        t.start(50)
        
        app.exec_()


if __name__ == '__main__':
    try:
        def io(running,q):
            t=0
            t0 = time.time()
            while running.is_set():
                s = np.sin(0.05*2 * np.pi * t)
                q.put((time.time()-t0,s*1+8,s*2+8,s*3+8,s*4+8,s*5+8,s*6+8,s*7+8))
                time.sleep(0.005)
                t+=0.01
            print("Done")
            
        #To stop IO thread
        run = threading.Event()
        run.set()
        
        #To let plot close after interrupt
#        can_close = threading.Event()
    
        # create the plot
        s = Superplot("somePlot")
        q, q_terminate = s.start()
        
        # start IO thread
        t = threading.Thread(target=io, args=(run,q))
        t.start()
        
        while True:
            time.sleep(1)
        run.clear()
    except KeyboardInterrupt:
        q_terminate.put(1)
        run.clear()
        
        
        