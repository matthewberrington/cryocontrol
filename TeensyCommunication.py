import serial
import numpy as np
import DualLogger
import sys
import time

class TeensyCommunication:

    def __init__(self, port='/dev/ttyACM0'):
        #get ready to grab data from teensy

        self.ser = serial.Serial(port, 115200)
        time.sleep(1)
        self.ser.reset_input_buffer()
        self.ser.reset_output_buffer()


    def receive_message(self):
        ser_bytes = self.ser.readline()
        message = ser_bytes.decode('utf-8')
        message = message.rstrip()
        DualLogger.comm_logger.info('Received: {}'.format(message))
        return message

    def send_message(self, message):
        self.ser.write(message.encode())
        DualLogger.comm_logger.info('Sent: {}'.format(message))

    def get_temperature_data(self):
        #set up arrays to capture a bunch of 
        VA_list = []
        VB_list = []
        VC_list = []
        VD_list = []
        VE_list = []
        VF_list = []
        VG_list = []
        

        #fill arrays with data!
        while True:
            # self.ser.reset_input_buffer()
            data = self.ser.readline() #get one temperature readout
            # If teensy says that this is the end of the temperature measurement, then exit loop
            if data == b'end_temperature\r\n':
                self.ser.reset_input_buffer()
                break

            data = list(map(int,data.decode().split(" "))) #split data into a list
            VA_list.append(data[0])
            VB_list.append(data[1])
            VC_list.append(data[2])
            VD_list.append(data[3])
            VE_list.append(data[4])
            VF_list.append(data[5])
            VG_list.append(data[6])        

        VA_list = np.array(VA_list)
        VB_list = np.array(VB_list)
        VC_list = np.array(VC_list)
        VD_list = np.array(VD_list)
        VE_list = np.array(VE_list)
        VF_list = np.array(VF_list)
        VG_list = np.array(VG_list)
        
        picker = np.zeros(800)
        for i in np.arange(800):
            if i % 40 < 20:
                picker[i] = 1

        VA_list = np.abs((VA_list[picker==1]+VA_list[picker==0])*3300.0/8191.0)
        VB_list = np.abs((VB_list[picker==1]+VB_list[picker==0])*3300.0/8191.0)
        VC_list = VC_list*3300.0/8191.0
        VD_list = VD_list*3300.0/8191.0
        VE_list = VE_list*3300.0/8191.0
        VF_list = VF_list*3300.0/8191.0
        VG_list = VG_list*3300.0/8191.0
        
        numMeasurements = len(VA_list)
        self.A = (np.mean(VA_list), np.std(VA_list)/np.sqrt(numMeasurements))
        self.B = (np.mean(VB_list), np.std(VB_list)/np.sqrt(numMeasurements))
        self.C = (np.mean(VC_list), np.std(VC_list)/np.sqrt(numMeasurements))
        self.D = (np.mean(VD_list), np.std(VD_list)/np.sqrt(numMeasurements))
        self.E = (np.mean(VE_list), np.std(VE_list)/np.sqrt(numMeasurements))
        self.F = (np.mean(VF_list), np.std(VF_list)/np.sqrt(numMeasurements))
        self.G = (np.mean(VG_list), np.std(VG_list)/np.sqrt(numMeasurements))

        return self.A, self.B, self.C, self.D, self.E, self.F, self.G

    def close(self):
        self.ser.close()
