
class DynamicPlot:        

    def adjustErrbarxy_assymetric(self,errobj, x, y, x_error_neg, x_error_pos, y_error_neg, y_error_pos):
        ln, (errx_top, errx_bot, erry_top, erry_bot), (barsx, barsy) = errobj
        x_base = x
        y_base = y
        ln.set_xdata(x_base)
        ln.set_ydata(y_base)


        xerr_top = x_base #+ x_error_pos
        xerr_bot = x_base #- x_error_neg
        yerr_top = y_base + y_error_pos
        yerr_bot = y_base - y_error_neg

        errx_top.set_xdata(xerr_top)
        errx_bot.set_xdata(xerr_bot)
        errx_top.set_ydata(y_base)
        errx_bot.set_ydata(y_base)

        erry_top.set_xdata(x_base)
        erry_bot.set_xdata(x_base)
        erry_top.set_ydata(yerr_top)
        erry_bot.set_ydata(yerr_bot)

        new_segments_x = [np.array([[xt, y], [xb,y]]) for xt, xb, y in zip(xerr_top, xerr_bot, y_base)]
        new_segments_y = [np.array([[x, yt], [x,yb]]) for x, yt, yb in zip(x_base, yerr_top, yerr_bot)]
        barsx.set_segments(new_segments_x)
        barsy.set_segments(new_segments_y)

    def __init__(self):

        #setup plot format        
        self.fig1 = plt.figure()
        self.ax_therm = self.fig1.add_subplot(111)
        # self.ax_diode = self.fig1.add_subplot(212)
        self.fig1.tight_layout()
        self.ax_therm.set_xlabel('Time (min)', fontsize='14')
        self.ax_therm.set_ylabel('Temperature (K)', fontsize='14')
        # self.ax_diode.set_xlabel('Time (min)', fontsize='14')
        # self.ax_diode.set_ylabel('Temperature (K)', fontsize='14')

        self.view_time = 60 # seconds of data to view at once

        self.ax_therm.set_yscale("log", nonposy='clip')
        # self.ax_diode.set_yscale("log", nonposy='clip')
        self.ax_therm.set_ylim([0.1,300])
        self.ax_therm.set_xlim([0,self.view_time])
        # self.ax_diode.set_ylim([0.1,100])
        # self.ax_diode.set_xlim([0,self.view_time])

        #add traces   
        self.A = self.ax_therm.errorbar(0, 0, 0, 0, color='C0', label='${}^3$He Head')
        self.B = self.ax_therm.errorbar(0, 0, 0, 0, color='C1', label='${}^4$He Head')
        # self.ax_therm.legend(loc='upper left')

        self.C = self.ax_therm.errorbar(0, 0, 0, 0, color='C2', label='Film Burner')
        self.D = self.ax_therm.errorbar(0, 0, 0, 0, color='C3', label='4He Pump')
        self.E = self.ax_therm.errorbar(0, 0, 0, 0, color='C4', label='3He Pump')
        self.F = self.ax_therm.errorbar(0, 0, 0, 0, color='C5', label='4He Switch')
        self.G = self.ax_therm.errorbar(0, 0, 0, 0, color='C6', label='3He Switch')
        self.ax_therm.legend(loc='upper left')

    def update_titles(self):
        #Do fancy text titles
        if self.R_He4_new == mVtoR_He4(3300):
            disp_Rerr_He4 = '$\infty$'
            disp_R_He4 = '$\infty$'
        else:
            disp_Rerr_He4 = sigfigs(self.Rerr_He4_new)
            disp_R_He4 = np.round(self.R_He4_new,-int(np.floor(np.log10(disp_Rerr_He4)-np.log10(2))))
        # disp_Rerr_He3 = sigfigs(self.Rerr_He3_new)
        # disp_R_He3 = np.round(self.R_He3_new,-int(np.floor(np.log10(disp_Rerr_He3)-np.log10(2))))

        disp_Terr_He4_neg = sigfigs(self.Terr_He4_new[0])
        disp_Terr_He4_pos = sigfigs(self.Terr_He4_new[1])
        disp_T_He4 = np.round(self.T_He4_new,-int(np.floor(np.log10(disp_Terr_He4_neg)-np.log10(2))))
        

        # disp_Terr_He3 = sigfigs(self.Terr_He3_new)
        # disp_T_He3 = np.round(self.T_He3_new,-int(np.floor(np.log10(disp_Terr_He3)-np.log10(2))))


        self.ax1.set_title('$^4$He: {} $\pm$ {} ohms'.format(disp_R_He4,disp_Rerr_He4), fontsize='18', fontweight='bold')
        self.ax2.set_title('$^4$He4: {} $\pm${}/{} K'.format(disp_T_He4,disp_Terr_He4_pos,disp_Terr_He4_neg), fontsize='18', fontweight='bold')
        # self.ax1.set_title('$^4$He: {} $\pm$ {} ohms, $^3$He: {} $\pm$ {} ohms'.format(disp_R_He4,disp_Rerr_He4,disp_R_He3,disp_Rerr_He3), fontsize='18', fontweight='bold')
        # self.ax2.set_title('$^4$He4: {} $\pm$ {} K, $^3$He: {} $\pm$ {} K'.format(disp_T_He4,disp_Terr_He4,disp_T_He3,disp_Terr_He3), fontsize='18', fontweight='bold')

    def refresh_plot(self, time_data, A_data, B_data, C_data, D_data, E_data, F_data, G_data):
        [A_data, A_neg, A_pos] = np.transpose(A_data)
        [B_data, B_neg, B_pos] = np.transpose(B_data)
        [C_data, C_neg, C_pos] = np.transpose(C_data)
        [D_data, D_neg, D_pos] = np.transpose(D_data)
        [E_data, E_neg, E_pos] = np.transpose(E_data)
        [F_data, F_neg, F_pos] = np.transpose(F_data)
        [G_data, G_neg, G_pos] = np.transpose(G_data)
        

        #Update data (with the new and the old points)          
        self.adjustErrbarxy_assymetric(self.A, time_data, A_data, 0, 0, A_neg, A_pos)
        self.adjustErrbarxy_assymetric(self.B, time_data, B_data, 0, 0, B_neg, B_pos)
        self.adjustErrbarxy_assymetric(self.C, time_data, C_data, 0, 0, C_neg, C_pos)
        self.adjustErrbarxy_assymetric(self.D, time_data, D_data, 0, 0, D_neg, D_pos)
        self.adjustErrbarxy_assymetric(self.E, time_data, E_data, 0, 0, E_neg, E_pos)
        self.adjustErrbarxy_assymetric(self.F, time_data, F_data, 0, 0, F_neg, F_pos)
        self.adjustErrbarxy_assymetric(self.G, time_data, G_data, 0, 0, G_neg, G_pos)
        
        #Scroll sideways
        current_time = time_data[-1]
        if current_time > self.view_time:
                self.ax_therm.set_xlim([current_time-self.view_time,current_time])
                # self.ax_diode.set_xlim([current_time-self.view_time,current_time])

        # Adjust vertical scale based on last three data point
        # if max(self.T_He4[-3::]+self.Terr_He4[-5::2])>=77:
        #     self.ax2.set_ylim([0,300])
        # elif max(self.T_He4[-3::]+self.Terr_He4[-5::2])>=4.2:
        #     self.ax2.set_ylim([0,50])
        # elif max(self.T_He4[-3::]+self.Terr_He4[-5::2])>=1:
        #     self.ax2.set_ylim([0,4.2])
        # else:
        #     self.ax2.set_ylim([0,1])
        
        #We need to draw *and* flush
        self.fig1.canvas.draw()
        self.fig1.canvas.flush_events()

