import logging
import time
import os

def setup_logger(name, log_file, level=logging.INFO):
    """Function setup as many loggers as you want"""

    formatter = logging.Formatter(fmt='%(asctime)s,%(levelname)s,%(message)s', datefmt = '%Y-%m-%d,%H:%M:%S')
    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

if not os.path.exists("logs"):
    os.makedirs("logs")
    

#Set up all the logging stuff
temp_logger = setup_logger('temperature', 'logs/temperature_log_'+time.strftime('%d%b%Y')+'.log')
temp_logger.info('He3_head,He4_head,Film_burner,He4_pump,He3_pump,He4_switch,He3_switch')
comm_logger = setup_logger('communication', 'logs/communication_logger'+time.strftime('%d%b%Y')+'.log')